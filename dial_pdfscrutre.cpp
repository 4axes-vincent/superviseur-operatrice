#include "dial_pdfscrutre.h"
#include "ui_dial_pdfscrutre.h"

#define MUTUELLE_ACTIVE         QString("Active")
#define MUTUELLE_INACTIVE       QString("Inactive")
#define MUTUELLE_INCONNU        QString("Inconnu")


Dial_PdfScrutre::Dial_PdfScrutre(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dial_PdfScrutre)
{
    ui->setupUi(this);
    this->setFixedSize (711,665);
    this->setWindowTitle ( ui->label->text () );

    importerPdfScrute();
}

Dial_PdfScrutre::~Dial_PdfScrutre()
{
    delete ui;
}






//----------------------------------------------------------------------------------------------------------------------
/*
  Importe les donn�es du PDFScrute (seulement l'activit� des mutuelles - 1/0)
  */
void Dial_PdfScrutre::importerPdfScrute(){

    Configuration *config = new Configuration();
    QString pathINI = config->getValue (QCoreApplication::applicationDirPath () % "/config.ini", "Path","ApiAuto")  % "/PDFScrute.ini";
    QStringList listeMutuelle = config->getListKey ( pathINI, "MUTUELLE");

    //On met � jour le nombre de ligne
    ui->tableWidget->setRowCount ( listeMutuelle.length () );
    ui->tableWidget->setColumnCount (2);

    //On boucle sur la liste des cl�s
    for( int i = 0 ; i < listeMutuelle.length () ; i++ ){

        QTableWidgetItem *itemActivite;
        QTableWidgetItem *itemMutuelle = new QTableWidgetItem( listeMutuelle.at (i) );

        //Ajout des donn�es dans la tableau
        ui->tableWidget->setItem (i,0, itemMutuelle);
        ui->tableWidget->item (i,0)->setTextAlignment (Qt::AlignCenter);

        //Condition sur l'acitivit� de la mutuelle [ 1 => ACTIVE / 0 => INACTIVE / Autre ou -1 => Inconnu ]
        if( config->getValue ( pathINI, "MUTUELLE",listeMutuelle.at (i) ) == "1"){

            itemActivite = new QTableWidgetItem( MUTUELLE_ACTIVE );
            itemActivite->setBackgroundColor ( Qt::green );
        }

        else if ( config->getValue ( pathINI, "MUTUELLE",listeMutuelle.at (i) ) == "0"){

            itemActivite = new QTableWidgetItem( MUTUELLE_INACTIVE );
            itemActivite->setBackgroundColor ( Qt::red );
        }
        else{

            itemActivite = new QTableWidgetItem( MUTUELLE_INCONNU );
            itemActivite->setBackgroundColor ( Qt::blue);
        }

        itemActivite->setFlags (Qt::ItemIsTristate);

        //Ajout des donn�es dans la tableau
        ui->tableWidget->setItem (i,1, itemActivite);
        ui->tableWidget->item (i,1)->setTextAlignment (Qt::AlignCenter);
    }

    ui->tableWidget->horizontalHeader ()->setResizeMode (QHeaderView::Stretch);
}














//----------------------------------------------------------------------------------------------------------------------
void Dial_PdfScrutre::debugger(QString titre, QString message){

    QMessageBox::warning (NULL,titre,message);
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur effectue une recherche, la tableau [tableWidget] s'actualise au changement de caract�re [arg1]
  */
void Dial_PdfScrutre::on_lineEdit_recherche_textChanged(const QString &arg1){

    for (int i = 0 ; i < ui->tableWidget->rowCount() ; i++) {

        if( ! ui->tableWidget->item(i,0)->text().contains( arg1.toUpper () ) )
            ui->tableWidget->setRowHidden(i,true);
        else
            ui->tableWidget->setRowHidden(i,false);
    }
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur une cellule dans la ligne [row] de la colonne [column]
  */
void Dial_PdfScrutre::on_tableWidget_cellClicked(int row, int column){

    mutuelleSelectionnee = ui->tableWidget->item (row, column)->text ();
    activiteMutuelle = ui->tableWidget->item (row, column + 1)->text ();

    ui->label_nomMutuelle->clear ();
    ui->label_nomMutuelle->setText ( mutuelleSelectionnee);

    if( activiteMutuelle == MUTUELLE_ACTIVE ){

        ui->radioButton_active->setChecked ( true );
        ui->radioButton_inactive->setChecked ( false );
    }
    else if ( activiteMutuelle == MUTUELLE_INACTIVE ){

        ui->radioButton_inactive->setChecked ( true );
        ui->radioButton_active->setChecked ( false );
    }
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Ferme la fen�tre
  */
void Dial_PdfScrutre::on_btn_quitter_clicked(){

    close ();

}











//----------------------------------------------------------------------------------------------------------------------
/*
  Valide le changement
  */
void Dial_PdfScrutre::on_btn_valider_clicked(){

    if( !ui->label_nomMutuelle->text ().isEmpty () ){

        Configuration *config = new Configuration();
        QString pathINI = config->getValue (QCoreApplication::applicationDirPath () % "/config.ini", "Path","ApiAuto")  % "/PDFScrute.ini";

        QString valeurActivite;

        if( ui->radioButton_active->isChecked () )
            valeurActivite = "1";

        else if( ui->radioButton_inactive->isChecked () )
            valeurActivite = "0";


        if( config->setValue ( pathINI, "MUTUELLE", ui->label_nomMutuelle->text (), valeurActivite) ){

            QMessageBox::information (NULL,"Modification","Activit� de la mutuelle " % ui->label_nomMutuelle->text () % " modifi�e");

            if( valeurActivite == "1" ){

                QTableWidgetItem *item = new QTableWidgetItem( MUTUELLE_ACTIVE );
                item->setBackgroundColor (Qt::green);

                ui->tableWidget->setItem ( ui->tableWidget->currentRow (), 1, item);
                ui->tableWidget->item (ui->tableWidget->currentRow (),1)->setTextAlignment (Qt::AlignCenter);
                item->setFlags (Qt::ItemIsSelectable);
            }
            else if( valeurActivite == "0" ){

                QTableWidgetItem *item = new QTableWidgetItem( MUTUELLE_INACTIVE );
                item->setBackgroundColor (Qt::red);

                ui->tableWidget->setItem ( ui->tableWidget->currentRow (), 1, item);
                ui->tableWidget->item (ui->tableWidget->currentRow (),1)->setTextAlignment (Qt::AlignCenter);
                item->setFlags (Qt::ItemIsSelectable);


            }

        }
        else
            debugger ("Erreur modification","Impossible de modifier l'activit� de la mutuelle " % ui->label_nomMutuelle->text () );
    }
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite afficher QUE les mutuelles actives
  */
void Dial_PdfScrutre::on_radioButton_mutuelleActive_clicked(){

    for (int i = 0; i < ui->tableWidget->rowCount(); ++i) {

        ui->tableWidget->setRowHidden(i,false);

         if( (ui->radioButton_mutuelleActive->isChecked () ) ){

            if(ui->tableWidget->item(i,1)->text() == "Inactive")
                ui->tableWidget->setRowHidden(i,true);
         }
         else
            ui->tableWidget->setRowHidden(i,false);
   }
}







//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite afficher QUE les mutuelles inatives
  */
void Dial_PdfScrutre::on_radioButton_mutuelleInactive_clicked(){

    for (int i = 0; i < ui->tableWidget->rowCount(); ++i) {

        ui->tableWidget->setRowHidden(i,false);

         if( (ui->radioButton_mutuelleInactive->isChecked () ) ){

            if(ui->tableWidget->item(i,1)->text() == "Active")
                ui->tableWidget->setRowHidden(i,true);
         }
         else
            ui->tableWidget->setRowHidden(i,false);
   }

}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite afficher TOUTES les mutuelles actives et inactives
  */
void Dial_PdfScrutre::on_radioButton_RAZ_clicked(){

    for (int i = 0; i < ui->tableWidget->rowCount(); ++i) {

        ui->tableWidget->setRowHidden(i,false);
    }
}
