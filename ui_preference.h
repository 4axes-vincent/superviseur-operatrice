/********************************************************************************
** Form generated from reading UI file 'preference.ui'
**
** Created: Tue 13. Jan 11:45:26 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCE_H
#define UI_PREFERENCE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Preference
{
public:
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *radioButton_firefox;
    QRadioButton *radioButton_googleChrome;
    QHBoxLayout *horizontalLayout;
    QPushButton *btn_valider_2;
    QPushButton *btn_annuler_2;

    void setupUi(QWidget *Preference)
    {
        if (Preference->objectName().isEmpty())
            Preference->setObjectName(QString::fromUtf8("Preference"));
        Preference->resize(325, 185);
        gridLayout_3 = new QGridLayout(Preference);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(Preference);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        radioButton_firefox = new QRadioButton(Preference);
        radioButton_firefox->setObjectName(QString::fromUtf8("radioButton_firefox"));

        horizontalLayout_2->addWidget(radioButton_firefox);

        radioButton_googleChrome = new QRadioButton(Preference);
        radioButton_googleChrome->setObjectName(QString::fromUtf8("radioButton_googleChrome"));

        horizontalLayout_2->addWidget(radioButton_googleChrome);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btn_valider_2 = new QPushButton(Preference);
        btn_valider_2->setObjectName(QString::fromUtf8("btn_valider_2"));

        horizontalLayout->addWidget(btn_valider_2);

        btn_annuler_2 = new QPushButton(Preference);
        btn_annuler_2->setObjectName(QString::fromUtf8("btn_annuler_2"));

        horizontalLayout->addWidget(btn_annuler_2);


        gridLayout_2->addLayout(horizontalLayout, 1, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(Preference);

        QMetaObject::connectSlotsByName(Preference);
    } // setupUi

    void retranslateUi(QWidget *Preference)
    {
        Preference->setWindowTitle(QApplication::translate("Preference", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Preference", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Pr\303\251f\303\251rence navigateur</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        radioButton_firefox->setText(QApplication::translate("Preference", "Mozilla FireFox", 0, QApplication::UnicodeUTF8));
        radioButton_googleChrome->setText(QApplication::translate("Preference", "Google Chrome", 0, QApplication::UnicodeUTF8));
        btn_valider_2->setText(QApplication::translate("Preference", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler_2->setText(QApplication::translate("Preference", "Annuler", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Preference: public Ui_Preference {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCE_H
