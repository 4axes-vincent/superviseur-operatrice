#ifndef CORRECTEUR_H
#define CORRECTEUR_H

#include <QWidget>
#include <QMessageBox>
#include <Configuration.h>
#include <QtNetwork/QHostInfo>
#include <QDateTime>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

namespace Ui {
class Correcteur;
}

class Correcteur : public QWidget
{
    Q_OBJECT
    
public:
    explicit Correcteur(QSqlDatabase db, QString nomOpe, QString VM, QString dateCo, QString heureCo, QWidget *parent = 0);
    ~Correcteur();
    
private slots:
    void on_btn_valider_clicked();

    void on_btn_annuler_clicked();

private:
    Ui::Correcteur *ui;

    QString date_connexion;
    QString heure_deconnexion;
    QString operatrice;
    QString numVM;
    QSqlDatabase db_correcteur;
};

#endif // CORRECTEUR_H
