/********************************************************************************
** Form generated from reading UI file 'dial_pdfscrutre.ui'
**
** Created: Fri 25. Mar 08:12:10 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIAL_PDFSCRUTRE_H
#define UI_DIAL_PDFSCRUTRE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dial_PdfScrutre
{
public:
    QTableWidget *tableWidget;
    QPushButton *btn_quitter;
    QPushButton *btn_valider;
    QLabel *label;
    QLineEdit *lineEdit_recherche;
    QFrame *line;
    QFrame *line_2;
    QLabel *label_modifMutuelle;
    QRadioButton *radioButton_active;
    QRadioButton *radioButton_inactive;
    QLabel *label_nomMutuelle;
    QRadioButton *radioButton_mutuelleActive;
    QRadioButton *radioButton_mutuelleInactive;
    QRadioButton *radioButton_RAZ;

    void setupUi(QWidget *Dial_PdfScrutre)
    {
        if (Dial_PdfScrutre->objectName().isEmpty())
            Dial_PdfScrutre->setObjectName(QString::fromUtf8("Dial_PdfScrutre"));
        Dial_PdfScrutre->resize(711, 629);
        tableWidget = new QTableWidget(Dial_PdfScrutre);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(20, 70, 671, 461));
        btn_quitter = new QPushButton(Dial_PdfScrutre);
        btn_quitter->setObjectName(QString::fromUtf8("btn_quitter"));
        btn_quitter->setGeometry(QRect(610, 580, 81, 23));
        btn_valider = new QPushButton(Dial_PdfScrutre);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(610, 550, 81, 23));
        label = new QLabel(Dial_PdfScrutre);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(300, 10, 131, 16));
        lineEdit_recherche = new QLineEdit(Dial_PdfScrutre);
        lineEdit_recherche->setObjectName(QString::fromUtf8("lineEdit_recherche"));
        lineEdit_recherche->setGeometry(QRect(440, 40, 251, 20));
        line = new QFrame(Dial_PdfScrutre);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(580, 540, 20, 81));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(Dial_PdfScrutre);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(10, 540, 20, 81));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        label_modifMutuelle = new QLabel(Dial_PdfScrutre);
        label_modifMutuelle->setObjectName(QString::fromUtf8("label_modifMutuelle"));
        label_modifMutuelle->setGeometry(QRect(240, 540, 91, 16));
        radioButton_active = new QRadioButton(Dial_PdfScrutre);
        radioButton_active->setObjectName(QString::fromUtf8("radioButton_active"));
        radioButton_active->setGeometry(QRect(240, 570, 82, 17));
        radioButton_inactive = new QRadioButton(Dial_PdfScrutre);
        radioButton_inactive->setObjectName(QString::fromUtf8("radioButton_inactive"));
        radioButton_inactive->setGeometry(QRect(360, 570, 82, 17));
        label_nomMutuelle = new QLabel(Dial_PdfScrutre);
        label_nomMutuelle->setObjectName(QString::fromUtf8("label_nomMutuelle"));
        label_nomMutuelle->setGeometry(QRect(330, 540, 181, 16));
        radioButton_mutuelleActive = new QRadioButton(Dial_PdfScrutre);
        radioButton_mutuelleActive->setObjectName(QString::fromUtf8("radioButton_mutuelleActive"));
        radioButton_mutuelleActive->setGeometry(QRect(20, 40, 101, 17));
        radioButton_mutuelleInactive = new QRadioButton(Dial_PdfScrutre);
        radioButton_mutuelleInactive->setObjectName(QString::fromUtf8("radioButton_mutuelleInactive"));
        radioButton_mutuelleInactive->setGeometry(QRect(130, 40, 111, 17));
        radioButton_RAZ = new QRadioButton(Dial_PdfScrutre);
        radioButton_RAZ->setObjectName(QString::fromUtf8("radioButton_RAZ"));
        radioButton_RAZ->setGeometry(QRect(250, 40, 51, 17));

        retranslateUi(Dial_PdfScrutre);

        QMetaObject::connectSlotsByName(Dial_PdfScrutre);
    } // setupUi

    void retranslateUi(QWidget *Dial_PdfScrutre)
    {
        Dial_PdfScrutre->setWindowTitle(QApplication::translate("Dial_PdfScrutre", "Form", 0, QApplication::UnicodeUTF8));
        btn_quitter->setText(QApplication::translate("Dial_PdfScrutre", "Quitter", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("Dial_PdfScrutre", "Valider", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Dial_PdfScrutre", "Administration PDFScrute", 0, QApplication::UnicodeUTF8));
        label_modifMutuelle->setText(QApplication::translate("Dial_PdfScrutre", "Modifier activit\303\251 : ", 0, QApplication::UnicodeUTF8));
        radioButton_active->setText(QApplication::translate("Dial_PdfScrutre", "Active", 0, QApplication::UnicodeUTF8));
        radioButton_inactive->setText(QApplication::translate("Dial_PdfScrutre", "Inactive", 0, QApplication::UnicodeUTF8));
        label_nomMutuelle->setText(QString());
        radioButton_mutuelleActive->setText(QApplication::translate("Dial_PdfScrutre", "Mutuelle active", 0, QApplication::UnicodeUTF8));
        radioButton_mutuelleInactive->setText(QApplication::translate("Dial_PdfScrutre", "Mutuelle inactive", 0, QApplication::UnicodeUTF8));
        radioButton_RAZ->setText(QApplication::translate("Dial_PdfScrutre", "R.A.Z", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Dial_PdfScrutre: public Ui_Dial_PdfScrutre {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIAL_PDFSCRUTRE_H
