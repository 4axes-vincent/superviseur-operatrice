#ifndef IDENTIFICATION_H
#define IDENTIFICATION_H

#include "Trieur.h"
#include "Configuration.h"
#include "Correcteur.h"

#include <QDialog>
#include <iostream>
#include <QMessageBox>
#include <QDate>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QProcess>
#include <QHostInfo>
#include <QStringBuilder>

//SQL
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

using namespace std;

namespace Ui {
class Identification;
}

class Identification : public QDialog
{
    Q_OBJECT
    
public:

    explicit Identification(QWidget *parent = 0);
    ~Identification();

private slots:

    void on_buttonBox_nomUser_accepted();
    void on_buttonBox_nomUser_rejected();

    //bool Identification::isConnect(QString user, QString dateCo);
    void creerTrace(QString user, QString hostname, QString dateCo, QString heureCo);
    void closeEvent(QCloseEvent *MouseEvent);
    void keyPressEvent(QKeyEvent * keyEvent);
    void listerOperatrice();

    void debugger(QString titre, QString information);

private:

    Ui::Identification *ui;
    Configuration *config;

    QString hostname;
    QString user;
    QString operatrice;

    QDate date;
    QDateTime time;

    int reponse;

    QStringList listOp;

    QList<QStringList> listeEnreg;

    QSqlDatabase db;
};

#endif // IDENTIFICATION_H
