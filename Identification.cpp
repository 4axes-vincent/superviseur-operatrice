#include "Identification.h"
#include "ui_Identification.h"
#include "mainwindow.h"

#define q2c(string) string.toStdString()

Identification::Identification(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Identification){

    ui->setupUi(this);
    this->setFixedSize(255,186);

    listerOperatrice();

    db = QSqlDatabase::addDatabase( config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "BDD", "database" ) );
    db.setHostName( config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "BDD", "hostName" ) );
    db.setUserName( config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "BDD", "userName" ) );
    db.setPassword( config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "BDD", "passWord" ) );
    db.setDatabaseName( QCoreApplication::applicationDirPath () + "\\" + QHostInfo::localHostName() );
}


//-------------------------------------------------------------------------------------------------------
Identification::~Identification(){

    delete ui;
}








//-------------------------------------------------------------------------------------------------------
void Identification::on_buttonBox_nomUser_accepted(){


    user = ui->comboBox_nomUser->currentText();

    //Met � jour la valeur de la cl� USER dans le groupe IDENTIFICATION -> Utilis� par l'API Manuelle
    config->setValue ( QCoreApplication::applicationDirPath () % "/config.ini", "Identification", "User", user);
    hostname = QHostInfo::localHostName();

    //Met � jour la BDD
    creerTrace(user, hostname, date.currentDate().toString("dd/MM/yyyy"), time.currentDateTime().toString("hh:mm:ss"));

    //IHM
    QMessageBox::information(NULL,"Identification","Vous �tes connect� sous " + user);

}












//-------------------------------------------------------------------------------------------------------
void Identification::on_buttonBox_nomUser_rejected(){

    reponse = QMessageBox::question(NULL,"Quitter l'application","�tes vous s�r de vouloir quitter l'application ?",QMessageBox::Yes, QMessageBox::No);

    if(reponse == QMessageBox::Yes)
            exit(0);    
    else{

        Identification *login = new Identification();
        login->show();
    }
}










//-------------------------------------------------------------------------------------------------------
void Identification::listerOperatrice(){


    /*
      POUR INSCRIRE LES NOMS & PRENOM
    */
    listOp.clear();

    QSettings settings(config->getPathINI()+"\\config.ini", QSettings::IniFormat);
    QString nbOperatrice = settings.value("4axes/nbOp","config").toString();

    for(int i = 1; i <= nbOperatrice.toInt() ; i++){

        listOp.push_back(settings.value("operatrice/op"+QString::number(i),"Default value ini").toString());
    }

    ui->comboBox_nomUser->addItems(listOp);
}



















//-------------------------------------------------------------------------------------------------------
void Identification::debugger(QString titre, QString information){

    QMessageBox::warning (NULL, titre, information);
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de cr�er la table TRACE si elle n'existe pas et d'insi�rer les donn�es
  */
void Identification::creerTrace(QString user, QString hostname, QString dateCo, QString heureCo){


    if( ! db.open())
        QMessageBox::warning(NULL,"Connexion base de donn�es",
                             "Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                             " | Erreur : " % db.lastError().text());

    else{

        QSqlQuery sqlCreateTable;
        QSqlQuery sqlQuery;
        QString requete;

        /*Cr�er la table TRACE si elle n'existe pas
            Col. 1 :        NOMOPE             = pr�nom nom op�ratrice
            Col. 2 :        VM                 = num�ro de la VM
            Col. 3 :        DATECO             = Date de connexion
            Col. 4 :        HEURECO              = Heure de connexion
            Col. 5 :        NBAPI              = Nombre de PEC trait�e par l'API
            Col. 6 :        NBNR               = Nombre de PEC Non R�conciliable
            Col. 7 :        NBMANU             = Nombre de PEC r�concil�e manuellement
            Col. 8 :        NBMANUNR           = Addition de NBNR et NBMANUNR
            Col. 9 :        NBNT               = Nombre de Non Traitable
            Col. 10 :        TOTAL              = Addition de NBNR, NBMANU et NBAPI
            Col. 11 :       NBTRIER            = Nombre de RPEC tri�e
        */

        sqlCreateTable = db.exec("CREATE TABLE IF NOT EXISTS TRACE ("
                                 " NOMOPE VARCHAR(20),"
                                 " VM VARCHAR(20),"
                                 " DATECO VARCHAR(10),"
                                 " HEURECO VARCHAR(8),"
                                 " NBAPI INTEGER(5),"
                                 " NBNR INTEGER(5),"
                                 " NBMANU INTEGER(5),"
                                 " NBMANUNR INTEGER(5),"
                                 " NBNT INTEGER(5),"
                                 " NBTRIER INTEGER(100),"
                                 " TOTAL INTEGER(5),"
                                 " );" );

        //Insertion des donn�es des BDD
        sqlQuery.clear ();
        requete.clear ();

        requete = "INSERT INTO TRACE VALUES('"+ user + "','" + hostname + "','" + dateCo + "','" + heureCo
                + "',0,0,0,0,0,0,0);";

        if ( !sqlQuery.exec(requete) )
            debugger ("Insertion donn�es",
                      "Impossible d'ex�cuter la requ�te [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );


        //Fermeture de la base de donn�es
        db.commit();
        db.removeDatabase ( QCoreApplication::applicationDirPath () + "\\" + QHostInfo::localHostName () );
        db.close();
    }
}














//-------------------------------------------------------------------------------------------------------
/*********************************************************************/
/************************** EVENT ************************************/
/*********************************************************************/


void Identification::closeEvent(QCloseEvent *MouseEvent){

    MouseEvent->ignore();
}











//-------------------------------------------------------------------------------------------------------
void Identification::keyPressEvent(QKeyEvent *keyEvent){

    keyEvent->ignore();
}











