#include "Trieur.h"

    Trieur::Trieur(QString path_GOOD, QString path_UNKNOWN, QProgressBar *bar){

        pathG = path_GOOD;
        pathU = path_UNKNOWN;
        progressBar = bar;

    }












//-------------------------------------------------------------------------------
void Trieur::trierListe (QList<QStringList> &liste){

    for( int k = 0 ; k < liste.length () ; k++){

        for(int i = 0 ; i < liste.length () - 1 ; i++){

            if( liste[i].at (3).toInt () < liste[i+1].at (3).toInt () )
                liste.swap (i,i+1);
        }
    }
}











//--------------------------------------------------------------------------------------------------------------------
QList<QStringList> Trieur::trierErreur (){

    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int i = 100 / entryList.count (); 

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier
        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [GoodNIN]
        nbGood = entryList.length ();


        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [UnknownNIN]
        nbUnknown = entryList.count ();


        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( i + progressBar->value ());

    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );
}










QList<QStringList> Trieur::trierErreurDate (QString date){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier
        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );
}









QList<QStringList> Trieur::trierErreurPeriode (QString date_1, QString date_2){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier
        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value () );
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );
}













//--------------------------------------------------------------------------------------------------------------------
QList<QStringList> Trieur::trierOK_MANUEL (){

    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int i = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [GoodNIN]
        nbGood = entryList.length ();


        dossier = pathU + "\\" + mutuelle  + "\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [UnknownNIN]
        nbUnknown = entryList.count ();


        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( i + progressBar->value ());
    }


    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );

}









QList<QStringList> Trieur::trierOK_MANUELDate (QString date){

    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }
        progressBar->setValue ( k + progressBar->value ());
    }


    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );


}














QList<QStringList> Trieur::trierOK_MANUELPeriode (QString date_1, QString date_2){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );


}























//--------------------------------------------------------------------------------------------------------------------
QList<QStringList> Trieur::trierOK (){

    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int i = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [GoodNIN]
        nbGood = entryList.length ();


//        dossier = pathU + "\\" + mutuelle + "\\OK";
//        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

//        //On r�cup�re le nombre de fichier dans ce dossier [UnknownNIN]
//        nbUnknown = entryList.count ();


        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( i + progressBar->value ());
    }


    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );

}














QList<QStringList> Trieur::trierOKDate (QString date){

    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbGood++;
        }

//        dossier = pathU + "\\" + mutuelle + "\\OK";
//        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

//        for(int i = 0 ; i < entryList.length () ; i++){

//            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
//                nbUnknown++;
//        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );

}




















QList<QStringList> Trieur::trierOKPeriode (QString date_1, QString date_2){

    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbGood++;
        }

//        dossier = pathU + "\\" + mutuelle + "\\OK";
//        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

//        for(int i = 0 ; i < entryList.length () ; i++){

//            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
//                nbUnknown++;
//        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );

}






//--------------------------------------------------------------------------------------------------------------------
QList<QStringList> Trieur::trierNR (){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int i = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [GoodNIN]
        nbGood = entryList.length ();


        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [UnknownNIN]
        nbUnknown = entryList.count ();


        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( i + progressBar->value ());
    }


    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );
}













QList<QStringList> Trieur::trierNRDate (QString date){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );
}











QList<QStringList> Trieur::trierNRPeriode (QString date_1, QString date_2){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood+nbUnknown != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );

}





//--------------------------------------------------------------------------------------------------------------------
QList<QStringList> Trieur::trierRecv (){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int i = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [GoodNIN]
        nbGood = entryList.length ();


//        dossier = pathU + "\\" + mutuelle;
//        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

//        //On r�cup�re le nombre de fichier dans ce dossier [UnknownNIN]
//        nbUnknown = entryList.count ();


        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood+nbUnknown ) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( i + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );


}



















QList<QStringList> Trieur::trierRecvDate (QString date){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") == date)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );



}













QList<QStringList> Trieur::trierRecvPeriode (QString date_1, QString date_2){


    uint nbGood = 0;
    uint nbUnknown = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    int k = 100 / entryList.count ();

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;
        nbUnknown = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbGood++;
        }

        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).lastRead ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).lastRead ().toString ("yyyyMMdd") <= date_2)
                nbUnknown++;
        }


        //Pour �viter d'ajouter des mutuelles o� il n'y a rien � r�concilier
        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );
            liste.push_back ( QString::number ( nbUnknown ) );
            liste.push_back ( QString::number ( nbGood) );

            listeStats.push_back (liste);
        }

        progressBar->setValue ( k + progressBar->value ());
    }

    progressBar->reset ();

    trierListe ( listeStats );
    return( listeStats );

}
