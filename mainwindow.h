#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "Trieur.h"
#include "Identification.h"
#include "Configuration.h"
#include "Rapport.h"
#include "Controleur.h"
#include "dial_pdfscrutre.h"


#include <QWidget>
#include <QMainWindow>
#include <QListWidgetItem>
#include <QDesktopServices>
#include <QString>
#include <QStringList>
#include <QStringListModel>
#include <QDesktopServices>
#include <QFileDialog>
#include <QUrl>
#include <QTimer>
#include <QDate>
#include <QWidget>
#include <QTextStream>
#include <QProcess>
#include <QTimer>
#include <QCloseEvent>
#include <QEvent>
#include <QSettings>
#include <QHostInfo>
#include <QStringBuilder>
#include <QFileInfoList>



#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void debugger(QString titre, QString information);

    void creerFichier(QString nom_oc);

private slots:

    void on_pushButton_refresh_clicked();

    void on_liste_oc_itemClicked(QListWidgetItem *item);

    void on_btn_UnknownNIN_clicked();

    void on_btn_good_clicked();

    void on_pushButton_refresh_OK_clicked();

    void on_actionCalculer_et_envoyer_triggered();

    void on_calendier_clicked(const QDate &date);

    void on_radioButton_tout_clicked();

    void on_radioButton_journee_clicked();

    void on_radioButton_periode_clicked();

    void on_btn_ou_poper_clicked();

    void on_btn_mutuelle_api_clicked();

    void on_bn_faxAttente_clicked();

    void on_btn_ouvrirMailQ_clicked();

    void on_actionGestion_PDFScrute_triggered();

    void on_actionMise_jour_triggered();

    void on_btn_relancerAPI_clicked();

private:

    Ui::MainWindow *ui;
    Configuration *config;

    QList<QStringList> listeResultat;

    QString dateSelectionnee_1;
    QString dateSelectionnee_2;
    QString OcSelectionne;
    QString nomOperatrice;

    int valeur_bouton;
    QString boutonSelect;

    QStringList listeOc;

    QString user;
    QString VM;
    QString dateCo;
    QString heureCo;
    QString heureDeco_effec;

    QString hostname;

    bool doubleConnexion;

    QTimer *timer;

    int clickID;

};

#endif // MAINWINDOW_H
