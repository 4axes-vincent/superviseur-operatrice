/********************************************************************************
** Form generated from reading UI file 'Correcteur.ui'
**
** Created: Tue 13. Jan 11:45:26 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CORRECTEUR_H
#define UI_CORRECTEUR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Correcteur
{
public:
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QLabel *label_titre;
    QLineEdit *lineEdit_date_deconnexion;
    QLabel *label_date_deconnexion;
    QLineEdit *lineEdit_heure_deconnexion;
    QLabel *label_heure_connexion;
    QLabel *label_date_connexion;
    QLabel *label_heure_connexion_2;
    QLineEdit *lineEdit_date_connexion;
    QLineEdit *lineEdit_heure_connexion;
    QLabel *label_exemple_date;
    QLabel *label_exemple_heure;

    void setupUi(QWidget *Correcteur)
    {
        if (Correcteur->objectName().isEmpty())
            Correcteur->setObjectName(QString::fromUtf8("Correcteur"));
        Correcteur->resize(382, 277);
        btn_valider = new QPushButton(Correcteur);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(190, 230, 75, 23));
        btn_annuler = new QPushButton(Correcteur);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(290, 230, 75, 23));
        label_titre = new QLabel(Correcteur);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(160, 30, 61, 16));
        lineEdit_date_deconnexion = new QLineEdit(Correcteur);
        lineEdit_date_deconnexion->setObjectName(QString::fromUtf8("lineEdit_date_deconnexion"));
        lineEdit_date_deconnexion->setGeometry(QRect(150, 170, 113, 20));
        label_date_deconnexion = new QLabel(Correcteur);
        label_date_deconnexion->setObjectName(QString::fromUtf8("label_date_deconnexion"));
        label_date_deconnexion->setGeometry(QRect(20, 170, 111, 16));
        lineEdit_heure_deconnexion = new QLineEdit(Correcteur);
        lineEdit_heure_deconnexion->setObjectName(QString::fromUtf8("lineEdit_heure_deconnexion"));
        lineEdit_heure_deconnexion->setGeometry(QRect(150, 200, 113, 20));
        label_heure_connexion = new QLabel(Correcteur);
        label_heure_connexion->setObjectName(QString::fromUtf8("label_heure_connexion"));
        label_heure_connexion->setGeometry(QRect(20, 200, 111, 16));
        label_date_connexion = new QLabel(Correcteur);
        label_date_connexion->setObjectName(QString::fromUtf8("label_date_connexion"));
        label_date_connexion->setGeometry(QRect(20, 100, 101, 16));
        label_heure_connexion_2 = new QLabel(Correcteur);
        label_heure_connexion_2->setObjectName(QString::fromUtf8("label_heure_connexion_2"));
        label_heure_connexion_2->setGeometry(QRect(20, 130, 101, 16));
        lineEdit_date_connexion = new QLineEdit(Correcteur);
        lineEdit_date_connexion->setObjectName(QString::fromUtf8("lineEdit_date_connexion"));
        lineEdit_date_connexion->setGeometry(QRect(150, 100, 113, 20));
        lineEdit_heure_connexion = new QLineEdit(Correcteur);
        lineEdit_heure_connexion->setObjectName(QString::fromUtf8("lineEdit_heure_connexion"));
        lineEdit_heure_connexion->setGeometry(QRect(150, 130, 113, 20));
        label_exemple_date = new QLabel(Correcteur);
        label_exemple_date->setObjectName(QString::fromUtf8("label_exemple_date"));
        label_exemple_date->setGeometry(QRect(280, 180, 71, 16));
        label_exemple_heure = new QLabel(Correcteur);
        label_exemple_heure->setObjectName(QString::fromUtf8("label_exemple_heure"));
        label_exemple_heure->setGeometry(QRect(280, 200, 61, 16));

        retranslateUi(Correcteur);

        QMetaObject::connectSlotsByName(Correcteur);
    } // setupUi

    void retranslateUi(QWidget *Correcteur)
    {
        Correcteur->setWindowTitle(QApplication::translate("Correcteur", "Form", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("Correcteur", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("Correcteur", "Annuler", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("Correcteur", "Correcteur", 0, QApplication::UnicodeUTF8));
        label_date_deconnexion->setText(QApplication::translate("Correcteur", "Date d\303\251connexion :", 0, QApplication::UnicodeUTF8));
        label_heure_connexion->setText(QApplication::translate("Correcteur", "Heure d\303\251connexion :", 0, QApplication::UnicodeUTF8));
        label_date_connexion->setText(QApplication::translate("Correcteur", "Date de connexion : ", 0, QApplication::UnicodeUTF8));
        label_heure_connexion_2->setText(QApplication::translate("Correcteur", "Heure de connexion :", 0, QApplication::UnicodeUTF8));
        label_exemple_date->setText(QApplication::translate("Correcteur", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-style:italic;\">JJ/MM/AAAA</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        label_exemple_heure->setText(QApplication::translate("Correcteur", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-style:italic;\">hh:mm:ss</span></p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Correcteur: public Ui_Correcteur {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CORRECTEUR_H
