#include "Configuration.h"

Configuration::Configuration(){

}




/*******************************************************************/
/**************** GETTEUR & SETTEUR CONFIG.INI *********************/
/*******************************************************************/



//---------------------------------------------------------------------------------------------------------
//Getteur -> Chemin d'acc�s du fichier Config.ini
QString Configuration::getPathINI(){

    QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
    QString pathINI16 = settings.value("path/config","Default value path config.ini 16").toString();
    return(pathINI16);

}





//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la valeur de cl� [key] dans le groupe [groupe] depuis le fichier de configuration [fichierINI]
  */
QString Configuration::getValue(QString fichierINI, QString groupe, QString key){

    QSettings settings( fichierINI, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    return( settings.value (key,"-1").toString () );

}














//----------------------------------------------------------------------------------------------------------------------
bool Configuration::setValue(QString fichierINI, QString groupe, QString key, QString value){

    QSettings settings( fichierINI, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    settings.setValue ( key, value );

    if( settings.value (key) == value )
        return( true );

    return( false );
}













//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la liste des cl�s du groupe [groupe] dans le fichier de configuration [fichierINI]
  */
QStringList Configuration::getListKey(QString fichierINI, QString groupe){

    QSettings settings( fichierINI, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    return( settings.allKeys () );
}
