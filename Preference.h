#ifndef PREFERENCE_H
#define PREFERENCE_H

#include "Configuration.h"

#include <QWidget>
#include <QMessageBox>

namespace Ui {
class Preference;
}

class Preference : public QWidget
{
    Q_OBJECT
    
public:
    explicit Preference(QWidget *parent = 0);
    ~Preference();
    
private slots:
    void on_btn_annuler_2_clicked();

    void on_btn_valider_2_clicked();

private:
    Ui::Preference *ui;
};

#endif // PREFERENCE_H
