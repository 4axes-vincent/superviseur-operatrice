#include "Rapport.h"
#include "ui_Rapport.h"

Rapport::Rapport(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Rapport)
{
    ui->setupUi(this);
    this->setFixedSize (494,402);
    this->setWindowTitle ("Rapport de volume");
    clickID = 0;

    ui->radioButton_tout->setChecked (true);
    ui->radioButton_journee->setChecked (false);
    ui->radioButton_periode->setChecked (false);

    ui->lineEdit_date1->setVisible (false);
    ui->lineEdit_date2->setVisible (false);

    ui->label_date1_au_date2->setVisible (false);

    pathG = config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax") % "/GoodNIN";
    pathU = config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax") % "/UnknownNIN";

    ui->progressBar->reset ();
}

Rapport::~Rapport(){

    delete ui;
}











//-------------------------------------------------------------------------------------------------------
void Rapport::on_calendrier_rapport_clicked(const QDate &date){


    if( ui->radioButton_journee->isChecked () ){

        date_debut = date;
        dateSelectionne1 = date.toString ("yyyyMMdd");
        ui->lineEdit_date1->setText (date.toString ("dd/MM/yyyy"));

    }
    else if( ui->radioButton_periode->isChecked () ){

        if( clickID == 0 ){

            date_debut = date;
            dateSelectionne1 = date.toString ("yyyyMMdd");
            ui->lineEdit_date1->setText (date.toString ("dd/MM/yyyy"));
            clickID = 1;
        }
        else if( clickID == 1 ){

            date_fin = date;
            dateSelectionne2 = date.toString ("yyyyMMdd");
            ui->lineEdit_date2->setText (date.toString ("dd/MM/yyyy"));
            clickID = 0;
        }
    }
}













//-------------------------------------------------------------------------------
void Rapport::on_radioButton_journee_clicked(){

    ui->lineEdit_date1->clear ();

    ui->lineEdit_date1->setVisible (true);
    ui->lineEdit_date2->setVisible (false);
    ui->label_date1_au_date2->setVisible (false);
}















//-------------------------------------------------------------------------------
void Rapport::on_radioButton_periode_clicked(){

    ui->lineEdit_date1->clear ();
    ui->lineEdit_date2->clear ();

    ui->lineEdit_date1->setVisible (true);
    ui->lineEdit_date2->setVisible (true);
    ui->label_date1_au_date2->setVisible (true);
}














//-------------------------------------------------------------------------------
void Rapport::on_radioButton_tout_clicked(){

    ui->lineEdit_date1->setVisible (false);
    ui->lineEdit_date2->setVisible (false);
}























//---------------------------------------------------------------------------------------------
void Rapport::on_btn_creer_rapport_clicked(){

    if( ui->radioButton_tout->isChecked () )
        listeStats = trier();

    else if( ui->radioButton_journee->isChecked () )
        listeStats = trier_ParDate (dateSelectionne1);

    else if( ui->radioButton_periode->isChecked () )
        listeStats = trier_ParPeriode (dateSelectionne1, dateSelectionne2);

    trierListe();

    statistiqueToCSV();
    envoyerRapport ();

    QMessageBox::information (NULL,"T�che termin�e","Rapport termin� avec succ�s");
}
















//-------------------------------------------------------------------------------
void Rapport::trierListe (){

    for( int k = 0 ; k < listeStats.length () ; k++){

        for(int i = 0 ; i < listeStats.length () - 1 ; i++){

            if( listeStats[i].at (6).toInt () < listeStats[i+1].at (6).toInt () )
                listeStats.swap (i,i+1);
        }
    }
}

















//-------------------------------------------------------------------------------------------------------
QList<QStringList> Rapport::trier (){


    uint nb_ok_manuel_good = 0;
    uint nb_ok_manuel_unknown = 0;
    uint nb_ok_good = 0;
    uint nb_nr_good = 0;
    uint nb_nr_uknown = 0;
    uint total = 0;
    uint nbDossier = 0;


    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfoList entryList;

    QDir dossier;

    QFileInfo info;

    info = pathG;
    dossier = info.absoluteFilePath ();
    entryList = dossier.entryInfoList ( QDir::Dirs | QDir::NoDotAndDotDot );
    nbDossier = entryList.length ();

    ui->label->setText ("Nombre de mutuelle � analyser : " + QString::number ( nbDossier ));

    int i = 100 / nbDossier;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);


    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nb_ok_manuel_good = 0;
        nb_ok_manuel_unknown = 0;
        nb_ok_good = 0;
        nb_nr_good = 0;
        nb_nr_uknown = 0;
        total = 0;

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        //Nombre de fichier dans OK
        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        nb_ok_good = entryList.length ();

        //Nombre de fichier dans NR
        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        nb_nr_good = entryList.length ();

        //Nombre de fichier dans OK_MANUEL
        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        nb_ok_manuel_good = entryList.length ();

        //Nombre de fichier dans NR [UNKNOWN]
        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        nb_nr_uknown = entryList.count ();

        //Nombre de fichier dans OK_MANUEL [UNKNOWN]
        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        nb_ok_manuel_unknown = entryList.count ();


        total = nb_ok_manuel_good + nb_ok_manuel_unknown + nb_nr_uknown + nb_nr_good + nb_ok_good;

        if( total != 0 ){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nb_ok_good ) );
            liste.push_back ( QString::number ( nb_ok_manuel_good ) );
            liste.push_back ( QString::number ( nb_nr_good) );
            liste.push_back ( QString::number ( nb_ok_manuel_unknown ) );
            liste.push_back ( QString::number ( nb_nr_uknown ) );
            liste.push_back ( QString::number ( total ) );

            listeStats.push_back (liste);
        }

        ui->progressBar->setValue (i + ui->progressBar->value ());
    }

    ui->progressBar->reset ();
    return( listeStats );
}






















//-------------------------------------------------------------------------------------------------------
QList<QStringList> Rapport::trier_ParDate(QString date){


    uint nb_ok_manuel_good = 0;
    uint nb_ok_manuel_unknown = 0;
    uint nb_ok_good = 0;
    uint nb_nr_good = 0;
    uint nb_nr_uknown = 0;
    uint total = 0;
    uint nbDossier = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfoList entryList;

    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    info = pathG;
    dossier = info.absoluteFilePath ();
    entryList = dossier.entryInfoList ( QDir::Dirs | QDir::NoDotAndDotDot );
    nbDossier = entryList.length ();

    int k = 100 / nbDossier;

    ui->label->setText ("Nombre de mutuelle � analyser : " + QString::number ( nbDossier ));

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nb_ok_manuel_good = 0;
        nb_ok_manuel_unknown = 0;
        nb_ok_good = 0;
        nb_nr_good = 0;
        nb_nr_uknown = 0;
        total = 0;

        //On r�cup�re les infos du dossier

        info = dirOC.next () ;

        mutuelle = info.baseName ();

        //Nombre de fichier dans OK
        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") == date)
                nb_ok_good++;
        }

        //Nombre de fichier dans NR
        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") == date)
                nb_nr_good++;
        }


        //Nombre de fichier dans OK_MANUEL
        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") == date)
                nb_ok_manuel_good++;
        }


        //Nombre de fichier dans NR [UNKNOWN]
        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") == date)
                nb_nr_uknown++;
        }


        //Nombre de fichier dans OK_MANUEL [UNKNOWN]
        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") == date)
                nb_ok_manuel_unknown++;
        }


        total = nb_ok_manuel_good + nb_ok_manuel_unknown + nb_nr_uknown + nb_nr_good + nb_ok_good;

        if( total != 0 ){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nb_ok_good ) );
            liste.push_back ( QString::number ( nb_ok_manuel_good ) );
            liste.push_back ( QString::number ( nb_nr_good) );
            liste.push_back ( QString::number ( nb_ok_manuel_unknown ) );
            liste.push_back ( QString::number ( nb_nr_uknown ) );
            liste.push_back ( QString::number ( total ) );

            listeStats.push_back (liste);
        }

        ui->progressBar->setValue (k + ui->progressBar->value ());
    }

    ui->progressBar->reset ();
    return( listeStats );
}












//-------------------------------------------------------------------------------------------------------
QList<QStringList> Rapport::trier_ParPeriode (QString date_1, QString date_2){


    uint nb_ok_manuel_good = 0;
    uint nb_ok_manuel_unknown = 0;
    uint nb_ok_good = 0;
    uint nb_nr_good = 0;
    uint nb_nr_uknown = 0;
    uint total = 0;
    uint nbDossier = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfoList entryList;

    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    info = pathG;
    dossier = info.absoluteFilePath ();
    entryList = dossier.entryInfoList ( QDir::Dirs | QDir::NoDotAndDotDot );
    nbDossier = entryList.length ();

    int k = 100 / nbDossier;

    ui->label->setText ("Nombre de mutuelle � analyser : " + QString::number ( nbDossier ));

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nb_ok_manuel_good = 0;
        nb_ok_manuel_unknown = 0;
        nb_ok_good = 0;
        nb_nr_good = 0;
        nb_nr_uknown = 0;
        total = 0;

        info = dirOC.next () ;
        mutuelle = info.baseName ();

        //Nombre de fichier dans OK
        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).created ().toString ("yyyyMMdd") <= date_2)
                nb_ok_good++;
        }

        //Nombre de fichier dans NR
        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).created ().toString ("yyyyMMdd") <= date_2)
                nb_nr_good++;
        }

        //Nombre de fichier dans OK_MANUEL
        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).created ().toString ("yyyyMMdd") <= date_2)
                nb_ok_manuel_good++;
        }

        //Nombre de fichier dans NR [UNKNOWN]
        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).created ().toString ("yyyyMMdd") <= date_2)
                nb_nr_uknown++;
        }

        //Nombre de fichier dans OK_MANUEL [UNKNOWN]
        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );
        for(int i = 0 ; i < entryList.length () ; i++){

            if( entryList.at (i).created ().toString ("yyyyMMdd") >= date_1 && entryList.at (i).created ().toString ("yyyyMMdd") <= date_2)
                nb_ok_manuel_unknown++;
        }


        total = nb_ok_manuel_good + nb_ok_manuel_unknown + nb_nr_uknown + nb_nr_good + nb_ok_good;

        if( total != 0 ){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nb_ok_good ) );
            liste.push_back ( QString::number ( nb_ok_manuel_good ) );
            liste.push_back ( QString::number ( nb_nr_good) );
            liste.push_back ( QString::number ( nb_ok_manuel_unknown ) );
            liste.push_back ( QString::number ( nb_nr_uknown ) );
            liste.push_back ( QString::number ( total ) );

            listeStats.push_back (liste);
        }

        ui->progressBar->setValue (k + ui->progressBar->value ());
    }


    ui->progressBar->reset ();
    return( listeStats );
}

















//-------------------------------------------------------------------------------------------------------
void Rapport::envoyerRapport (){

    QString host = config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini","config_mail", "host");
    QString from = config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini","config_mail", "from");
    QString to = config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini","config_mail", "email");

    system("C:\\Postie\\postie.exe -host:" + host.toLocal8Bit ()
           + " -from:" + from.toLocal8Bit ()
           + " -to:" + to.toLocal8Bit ()
           + " -s:\"Rapport de volume " + VM.toLocal8Bit () + "\" -nomsg -a:"+ nomFichier.toLocal8Bit () + "");

    backuperFichier();

}










//-------------------------------------------------------------------------------------------------------
void Rapport::statistiqueToCSV (){

    QHostInfo host;
    QDateTime dateTime;
    VM = host.localHostName ();
    nomFichier = "rapport_" + VM + "_" +  dateTime.currentDateTime ().toString ("yyyyMMdd_hhmmss").append (".csv");

    QFile fichierSortie(nomFichier);
    if( !fichierSortie.open (QIODevice::Text | QIODevice::WriteOnly) )
        QMessageBox::warning (NULL,"Ouverture fichier","Impossible d'ouvrir le fichier rappport");
    else{

        QTextStream fluxSortant(&fichierSortie);

        fluxSortant << VM << ";" << date_debut.toString ("dd/MM/yyyy") << ";" << date_fin.toString ("dd/MM/yyyy") << endl;
        fluxSortant << ";;GOODNIN " << " ;;; " << "UNKNOWNIN " << endl;
        fluxSortant << "Organisme compl�mentaire" << ";" << "OK" << ";" << "OK_MANUEL" << ";" << "NR"  << ";;" << "OK_MANUEL" << ";" << "NR" <<  ";" << "Total" << endl;

        for(int i = 0 ; i < listeStats.length () ; i++){

            fluxSortant << listeStats[i][0]  << ";";
            fluxSortant << listeStats[i][1]  << ";";
            fluxSortant << listeStats[i][2]  << ";";
            fluxSortant << listeStats[i][3]  << ";;";
            fluxSortant << listeStats[i][4]  << ";";
            fluxSortant << listeStats[i][5]  << ";";
            fluxSortant << listeStats[i][6]  << ";" << endl;
        }
    }
}


//-------------------------------------------------------------------------------------------------------
void Rapport::backuperFichier (){

    QFile::copy (QCoreApplication::applicationDirPath () + "\\" + nomFichier, QCoreApplication::applicationDirPath () + "\\BackupRapport\\" + nomFichier);
    QFile::remove (QCoreApplication::applicationDirPath () + "\\" + nomFichier);

}
