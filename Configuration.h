#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QString>
#include <QStringList>
#include <QSettings>
#include <QCoreApplication>

class Configuration
{
public:
    Configuration();

    QString getPathINI();

    QString getValue(QString fichierINI, QString groupe, QString key);
    bool setValue(QString fichierINI, QString groupe, QString key, QString value);
    QStringList getListKey(QString fichierINI, QString groupe);
};

#endif // CONFIGURATION_H
