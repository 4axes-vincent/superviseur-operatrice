#include "Trieur.h"
#include <QApplication>
#include <QtGui>
#include <mainwindow.h>
#include <Identification.h>
#include <QMessageBox>

using namespace std;

/*Version 1 :   D�velopp� par Nicolas SHAO
                Language : JAVA

  Version 2 :   Red�velopp� par Nicolas SHAO & Vincent PARRAMON
                Language : C++ / Qt 4
                Am�lioration : R�difinition des m�thodes suivantes :

                        int aRecoManu();
                        int listerPDF(QString path);
                        int afficherGoodNIN(QString item, int valeurItem);
                        int afficherUnknownNIN(QString item,int valeurItem);
                        int ReconParApi();
                        int ReconManu();
                        int nonRecon();
                        int listerRepertoire(QString path);

                        void tri_rapide(vector<int> vNb,QStringList &listeOc,int debut, int fin);
                        void echanger(vector<int> &vNb,QStringList &listeOc, int gauche, int droite);
                        void verifierStructureGood();
                        void verifierStructureUnknow();
                        void setDate(QString date1,QString date2);

                        QStringList listeOcARecon();
                        QStringList listeOcReconApi();
                        QStringList listeOcReconManu();
                        QStringList listeOcNonRecon();
                        QString getDateFichier(QString nomFichier);
                        QString getPath();
                        QStringList trier(QStringList &listeOc, int valeur_btn);

                        bool OcExist (QStringList vOC,QString oc);
                        bool dossierExiste(QString item);
                        bool siDossierExiste(QString path, QString dossierAChercher);

                        + Am�lioration de l'aspect graphique / ergonomique
                        + Am�lioration performance


    Version 3 :     Red�velopp� par Vincent PARRAMON
                    Language : C++ / Qt 4
                    Am�lioration des m�thodes suivantes :


                        void MainWindow::on_refresh_all_clicked();
                        void on_pushButton_refresh_reconApi_clicked();
                        void on_pushButton_refresh_reconManu_clicked();
                        void on_pushButton_refresh_nonRecon_clicked();

                        int listerRepertoire(QString path);

                        bool dossierExiste(QString item);

                        void verifierStructureGood();
                        void verifierStructureUnknow();

                        void setDate(QString date1,QString date2);

                        bool OcExist (QStringList listeOc,QString oc);

                    Suppression des m�thodes suivantes :


                        int aRecoManu();
                        int ReconParApi();
                        int ReconManu();
                        int nonRecon();

                        QStringList listeOcARecon();
                        QStringList listeOcReconApi();
                        QStringList listeOcReconManu();
                        QStringList listeOcNonRecon();


                    Cr�ation des m�thodes suivantes :

                        int parcoursPoperfaxAll(QString dir, QStringList &listeOc, QVector<int> &vNbPdf);
                        int parcoursPoperfaxDay(QString dir, QStringList &listeOc, QVector<int> vNbPdf);
                        int parcoursPoperfaxPeriod(QString dir, QStringList &listeOc, QVector<int> vNbPdf);




                        Evolution : Utilisation de fonctionnalit�s appropri�es dans le parcours des dossiers et sous dossier (QIterator)
                                    Am�riolation de la lisibilit� du code
                                    Optimisation des m�thodes existantes



    Version 3.5 :   Red�velopp� par Vincent PARRAMON
                    Language : C++ / Qt 4
                    Besoin �voqu� par : Marie-Eve JEAN & Didier DUMONT
                    Am�lioration futurs :

                        Zone d'identification --> S'identifier pour travailler
                                Permet de connaitre le temps de travail de chaque employ� (temps d'activit� de chaque SuperViseur)



    Version 8.5 :   Red�velopp� par Vincent PARRAMON
                    Language : C++ / Qt 5

                    Am�liorations :

                        Plus d'ouverture de dossier (Good ou Unknown)
                        Ouverture de l'API manuelle via les boutons "GoodNIN" et "UnknownNIN"
                        Ajout de cl�/valeur dans le fichier de configuration + acc�s via la class Configuration :
                            - cl� "api" dans [Path] -> Chemin d'acc�s pour l'API Manuelle
                            - cl� "Utilisateur" dans [Identification] -> Utilisateur connect� au SuperViseur � l'instant t


  */
//Version

int main(int argc, char * argv[])
{

    QApplication app(argc,argv);

    MainWindow fenetre;
    fenetre.show();

    Identification login;
    login.show ();

    return app.exec();
}
