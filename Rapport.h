#ifndef RAPPORT_H
#define RAPPORT_H

#include "Configuration.h"

#include <QWidget>
#include <QDate>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QtNetwork/QHostInfo>
#include <QMessageBox>
#include <QTextStream>
#include <QStatusBar>
#include <QStringBuilder>

namespace Ui {
class Rapport;
}

class Rapport : public QWidget
{
    Q_OBJECT
    
public:
    explicit Rapport(QWidget *parent = 0);
    ~Rapport();

    void trierListe();
    void statistiqueToCSV();
    void envoyerRapport();

    void backuperFichier();

    QList<QStringList> trier();
    QList<QStringList> trier_ParDate(QString date);
    QList<QStringList> trier_ParPeriode(QString date_1, QString date_2);




private slots:
    void on_calendrier_rapport_clicked(const QDate &date);

    void on_radioButton_journee_clicked();

    void on_radioButton_periode_clicked();

    void on_radioButton_tout_clicked();

    void on_btn_creer_rapport_clicked();

private:
    Ui::Rapport *ui;
    Configuration *config;
    QStatusBar *statusBar;

    QString pathG;
    QString pathU;
    QString dateSelectionne1;
    QString dateSelectionne2;
    QString VM;
    QString nomFichier;

    QDate date_debut;
    QDate date_fin;

    QList<QStringList> listeStats;


    int clickID;
};

#endif // RAPPORT_H
