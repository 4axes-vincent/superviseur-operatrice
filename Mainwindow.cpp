#include "mainwindow.h"
#include "ui_Mainwindow.h"
#include "Trieur.h"
#include "Identification.h"

#define TEMPS_MAX_INACTIVITE            int(1800000)
#define PATH_FICHIER_CONFIGURATION      QString(QCoreApplication::applicationDirPath () % "/config.ini")

//Constructeur
//Initialisation de l'ensemble du SuperViseur v3.8
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){

    ui->setupUi(this);
    ui->statusBar->showMessage("Pr�t");
    this->setWindowTitle("4axes : SuperViseur v3.8.5");
    this->setFixedSize(693,566);


    //Initialisation de la ProgressBar
    ui->progressBar->reset();

    //Tri sur TOUT par d�faut
    ui->radioButton_tout->setChecked (true);

    //Calendrier non visible par d�faut
    ui->calendier->setVisible(false);

    //line_edit date_1 et date_2 + label "au" non visible
    ui->line_date1->setVisible(false);
    ui->line_date2->setVisible(false);
    ui->label_Au->setVisible(false);

    clickID = 0;
    valeur_bouton = 0;

    timer = new QTimer();
    timer->start ( TEMPS_MAX_INACTIVITE );
    connect (timer, SIGNAL(timeout()), this, SLOT(close()));

    ui->btn_ouvrirMailQ->setVisible (false);
}








//-----------------------------------------------------------------------------------------------------------------------------------
//Destructeur
MainWindow::~MainWindow(){

    config->setValue ( PATH_FICHIER_CONFIGURATION, "Identification", "User", "NULL");

    delete ui;
}












//-----------------------------------------------------------------------------------------------------------------------------------
void MainWindow::debugger(QString titre, QString information){

    QMessageBox::warning (NULL,titre, information);
}

















//----------------------------------------------------------------------------------------------------------------------
/*  SIGNAL => clicked()
    Actualise la premi�re ligne / la liste de mutuelle / nombre de GoodNIN Et UnknownNIN
*/
void MainWindow::on_pushButton_refresh_clicked(){

    timer->start (TEMPS_MAX_INACTIVITE);

    listeOc.clear();
    listeResultat.clear ();
    ui->liste_oc->clear ();

    boutonSelect = "ERREUR";

    ui->btn_ouvrirMailQ->setVisible (false);

    int totalAReconcilier = 0;

    Trieur *trier = new Trieur( config->getValue ( PATH_FICHIER_CONFIGURATION, "Path", "poperfax")
                               % "/GoodNIN",
                               config->getValue ( PATH_FICHIER_CONFIGURATION, "Path", "poperfax")
                                % "/UnknownNIN", ui->progressBar);

    if( ui->radioButton_tout->isChecked ()  )
        listeResultat = trier->trierErreur ();

    else if( ui->radioButton_journee->isChecked () )
        listeResultat = trier->trierErreurDate ( dateSelectionnee_1 );

    else if( ui->radioButton_periode->isChecked () )
        listeResultat = trier->trierErreurPeriode ( dateSelectionnee_1, dateSelectionnee_2);

    for(int i = 0 ; i < listeResultat.length () ; i++){

        listeOc.push_back (listeResultat[i][0]);
        totalAReconcilier += listeResultat[i].at (3).toInt ();
    }

    ui->lineEdit_arecon->setText (QString::number (totalAReconcilier));
    ui->liste_oc->addItems (listeOc);
}
















//----------------------------------------------------------------------------------------------------------------------
/*
    SIGNAL => itemClicked(QListWidgetItem *item)
    Proc�dure : Change la valeur de "GoodNIN" et "UnknownNIN
    En fonction de la mutuelle s�lectionn�e
*/
void MainWindow::on_liste_oc_itemClicked(QListWidgetItem *item){

    timer->start (TEMPS_MAX_INACTIVITE);

    OcSelectionne = item->text ();

    for(int i = 0 ; i < listeResultat.length () ; i++){

        if( listeResultat.at (i).contains ( OcSelectionne) ){

            ui->line_good->setText (listeResultat.at (i).at (1));
            ui->line_unknown->setText (listeResultat.at (i).at (2));
        }
    }
}


















//----------------------------------------------------------------------------------------------------------------------
/*
    SIGNAL => clicked()
    Proc�dure : Change le chemin d'acc�s de GoodNIN en fonction
    de la ligne actualiser
    */
void MainWindow::on_btn_good_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    if( !OcSelectionne.isEmpty () ){

        QProcess processus;
        QStringList listeParametre;
        QString path = config->getValue ( PATH_FICHIER_CONFIGURATION, "Path", "api");

        if( boutonSelect == "ERREUR" ){


            listeParametre << path % "\\GoodNIN\\" % OcSelectionne % "\\ERREUR" ;
            processus.startDetached (path % "\\API_Recon_Manuel.exe", listeParametre);

        }
        else if( boutonSelect == "reception" )
            QDesktopServices::openUrl(QUrl("file:///"+ config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax")
                                           % "/GoodNIN/" % OcSelectionne,
                                           QUrl::TolerantMode) );
    }
    else
        QMessageBox::warning(NULL,"SuperViseur","Veuillez s�lectionner une mutuelle");
}






















//----------------------------------------------------------------------------------------------------------------------
/*
    SIGNAL => clicked()
    Proc�dure : Change le chemin d'acc�s de GoodNIN en fonction
    de la ligne actualiser
    */
void MainWindow::on_btn_UnknownNIN_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    //Si la variable de l'OC n'est pas vide
    if( !OcSelectionne.isEmpty () ){

        QProcess processus;
        QStringList listeParametre;
        QString path = config->getValue ( PATH_FICHIER_CONFIGURATION, "Path", "api");

        if( boutonSelect == "ERREUR" ){

            listeParametre << path % "\\UnknownNIN\\" % OcSelectionne;
            processus.startDetached ( path % "\\API_Recon_Manuel.exe", listeParametre);
        }
        else if ( boutonSelect == "reception" )
            QDesktopServices::openUrl(QUrl("file:///"+ config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax")
                                           % "/UnknownNIN/" % OcSelectionne,QUrl::TolerantMode));
    }
    else
        QMessageBox::warning(NULL,"Nom mutuelle","Veuillez s�lectionner une mutuelle");
}










//----------------------------------------------------------------------------------------------------------------------
/*
    SIGNAL => clicked()
    Proc�dure : actualise la ligne "R�ceptionn�"
    */
void MainWindow::on_pushButton_refresh_OK_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    listeOc.clear();
    listeResultat.clear ();
    ui->liste_oc->clear ();

    boutonSelect = "reception";

    ui->btn_ouvrirMailQ->setVisible (false);

    int total = 0;

    Trieur *trier = new Trieur(config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax") % "/GoodNIN",
                              config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax") % "/UnknownNIN",
                               ui->progressBar);

    if( ui->radioButton_tout->isChecked ()  )
        listeResultat = trier->trierRecv ();

    else if( ui->radioButton_journee->isChecked () )
        listeResultat = trier->trierRecvDate ( dateSelectionnee_1 );

    else if( ui->radioButton_periode->isChecked () )
        listeResultat = trier->trierRecvPeriode ( dateSelectionnee_1, dateSelectionnee_2 );

    for(int i = 0 ; i < listeResultat.length () ; i++){

        listeOc.push_back (listeResultat[i][0]);
        total += listeResultat[i].at (3).toInt ();
    }

    ui->lineEdit_OK->setText (QString::number (total));
    ui->liste_oc->addItems (listeOc);

}



























//---------------------------------------------------------------------------------------------
/*
  Slot sur le bouton radio "Tout" li� au calendrier
  */
void MainWindow::on_radioButton_tout_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    ui->calendier->setVisible (false);
    ui->line_date1->setVisible (false);
    ui->line_date2->setVisible (false);
    ui->label_Au->setVisible (false);
}









//---------------------------------------------------------------------------------------------
/*
    Slot sur le bouton radio "Journ�e" li� au calendrier
    */
void MainWindow::on_radioButton_journee_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    QDate date;

    ui->calendier->setVisible (true);
    ui->line_date1->setVisible (true);
    ui->line_date2->setVisible (false);
    ui->label_Au->setVisible (false);

    dateSelectionnee_1 = date.currentDate ().toString ("yyyyMMdd");
    ui->line_date1->setText ( date.currentDate ().toString ("dd/MM/yyyy") );
}










//---------------------------------------------------------------------------------------------
/*
    Slot sur le bouton radio "P�riode" li� au calendrier
    */
void MainWindow::on_radioButton_periode_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    ui->calendier->setVisible (true);
    ui->line_date1->setVisible (true);
    ui->line_date2->setVisible (true);
    ui->label_Au->setVisible (true);

}













//---------------------------------------------------------------------------------------------
/*
  Slot sur le calendrier lorsque l'on s�lectionne une ou plusieurs date
  */
void MainWindow::on_calendier_clicked(const QDate &date){

    timer->start ( TEMPS_MAX_INACTIVITE );

    //Si journ�e activ�e
    if( ui->radioButton_journee->isChecked () ){

        dateSelectionnee_1 = date.toString ( "yyyyMMdd" );
        ui->line_date1->setText ( date.toString ("dd/MM/yyyy") );
    }
    //Si p�riode activ�e
    else if( ui->radioButton_periode->isChecked () ){

        //Si aucun clique n'a �t� fait
        if( clickID == 0 ){

            dateSelectionnee_1 = date.toString ( "yyyyMMdd" );
            ui->line_date1->setText ( date.toString ("dd/MM/yyyy") );
            clickID = 1;
        }
        //Lorsqu'un clique a d�j� �t� fait
        else if( clickID == 1 ){

            dateSelectionnee_2 = date.toString ( "yyyyMMdd" );
            ui->line_date2->setText ( date.toString ("dd/MM/yyyy") );
            clickID = 0;
        }
    }
}





























































































//----------------------------------------------------------------------------------------------------------------------
/*
  Slot pour la cr�ation des rapports (statistique volume)
  */
void MainWindow::on_actionCalculer_et_envoyer_triggered(){

    Rapport *report = new Rapport();
    report->show ();
}


































//----------------------------------------------------------------------------------------------------------------------
/*
  Slot : indique sur quelle VM le Popper s'ex�cute
  */
void MainWindow::on_btn_ou_poper_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    QStringList filtre;
    filtre << "*blo";
    QFileInfo infoFic;

    QDirIterator iterateur( config->getValue ( PATH_FICHIER_CONFIGURATION, "Path", "system_16"),
                            filtre, QDir::Files);

    while( iterateur.hasNext () ){

        infoFic = iterateur.next ();
        ui->label_poper->setText (infoFic.baseName ().mid (0,2));
    }
}


















































//----------------------------------------------------------------------------------------------------------------------
/*
  Slot : Affiche sur quelle mutuelle l'API est en cours de r�conciliation
  */
void MainWindow::on_btn_mutuelle_api_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );
    QString pathPdfScrute = config->getValue ( PATH_FICHIER_CONFIGURATION, "Path", "PDFScrute") % "/PDFScrute.ini";
    ui->label_mutuelle_API->setText ( config->getValue ( pathPdfScrute, "TRAITEMENT", "En_cours" ));
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur "Fax en attente"
  */
void MainWindow::on_bn_faxAttente_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    boutonSelect = "faxRecu";
    ui->btn_ouvrirMailQ->setVisible (true);

    QDir dossier( config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax") % "/MailQ");

    //On compte le nombre de fichier dans le dossier cible
    QFileInfoList entryList = dossier.entryInfoList (QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    ui->lineEdit_faxAttente->setText ( QString::number ( entryList.count() ) );

}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ouvrir le dossier MailQ dans PoperFax
  */
void MainWindow::on_btn_ouvrirMailQ_clicked(){

    timer->start ( TEMPS_MAX_INACTIVITE );

    QDesktopServices::openUrl(QUrl("file:///"+ config->getValue ( QCoreApplication::applicationDirPath() % "/config.ini", "path", "poperfax") % "/MailQ",
                                   QUrl::TolerantMode));

}














//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite g�rer les informations du PDFScrute
  */
void MainWindow::on_actionGestion_PDFScrute_triggered(){

    Dial_PdfScrutre *PdfScrute = new Dial_PdfScrutre();
    PdfScrute->show ();

}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite mettre � jour son superViseur
  */
void MainWindow::on_actionMise_jour_triggered(){

    QProcess process;
    QStringList argument;
    argument << "superViseur";
    process.startDetached (QCoreApplication::applicationDirPath () % "/udapter.exe", argument);
}








//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de relancer l'API si celle-ci a plant�
  */
void MainWindow::on_btn_relancerAPI_clicked(){

    system("taskkill /F /IM ClientREST.exe");
    system("start " + config->getValue (QCoreApplication::applicationDirPath () + "/config.ini", "Path", "ApiAuto").toLocal8Bit ()
           + "/ClientREST.exe");
}






















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::creerFichier(QString nom_oc){

    QString nom_pc = QHostInfo::localHostName();
    QString texte;
    QString ancienneLigne = "<input type=\"hidden\" name=\"adresseMail\" value=\"\">";
    QString nouvelleLigne;

    if( nom_oc == "322215021_SWISS" || nom_oc == "432701639_ALMERYS_AND_CO" )
            nouvelleLigne = "<input type=\"hidden\" name=\"adresseMail\" value="+ OcSelectionne.mid (0, OcSelectionne.indexOf ("_")).append ( "x" + nom_pc.mid ( nom_pc.length ()-2,2) + "@4axes.net") + ">";
    else
        nouvelleLigne = "<input type=\"hidden\" name=\"adresseMail\" value="+ OcSelectionne.mid (0, OcSelectionne.indexOf ("_")).append ("@4axes.net") + ">";


    QFile fichierEntree(QCoreApplication::applicationDirPath()+ "\\connexion.html");
    // On ouvre notre fichier en lecture seule et on v�rifie l'ouverture
    if ( ! fichierEntree.open(QIODevice::ReadOnly | QIODevice::Text) )
        QMessageBox::information(NULL,"Probl�me ouverture fichier connexion.html","Le fichier connexion.html n'a pas pu �tre ouvert !");

    else{

        //<input type="hidden" name="adresseMail" value="">
        // Cr�ation d'un objet QTextStream � partir de notre objet QFile
        QTextStream fluxEntrant(&fichierEntree);
        texte = fluxEntrant.readAll ();
        fichierEntree.close ();
    }

    texte.replace (ancienneLigne, nouvelleLigne);

    QFile fichierSortie(QCoreApplication::applicationDirPath()+ "\\connexion_true.html");
    if ( ! fichierSortie.open(QIODevice::WriteOnly | QIODevice::Text) )
        QMessageBox::information(NULL,"Probl�me ouverture fichier connexion_true.html","Le fichier connexion.html n'a pas pu �tre cr�� !");

    else{

        QTextStream fluxSortant(&fichierSortie);
        fluxSortant << texte << endl;
        fichierSortie.close ();



        system("start connexion_true.html");
//        process.startDetached ("lancerNavigateur.bat");
//        system("start " + path.toLocal8Bit () + "-new-windows " + ( QCoreApplication::applicationDirPath () + "\\connexion_true.html").toLocal8Bit () );
    }
}
