#include "stats.h"
#include "ui_stats.h"

Stats::Stats(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Stats){

    ui->setupUi(this);

    this->setWindowTitle("Statistique r�ception");
}




//-------------------------------------------------------------------------------------------------------
Stats::~Stats(){

    delete ui;
}




//-------------------------------------------------------------------------------------------------------
void Stats::on_btn_generateGraph_clicked(){

    if(ui->lineEdit_date->text().isEmpty()){

        QMessageBox::warning(NULL,"Date inconnu","Veuillez s�lectionner une date !");

    }
    else{

        genererGraphique();

    }
}




//-------------------------------------------------------------------------------------------------------
void Stats::genererGraphique(){

    QString resultat = "[";
    QString cumul;
    QStringList abscisse;
    abscisse << "0700" << "0800" << "0900" << "1000" << "1100" << "1200" << "1300" << "1400" << "1500" << "1600" << "1700" << "1800" << "1900" << "2000" << "2100";

    for(int k = 0; k < abscisse.length() ; k++){

        if(abscisse.at(k) != "2100"){

            cumul = calculerStats(abscisse.at(k), abscisse.at(k+1));
            resultat += cumul + ",";
        }
    }

    resultat += "]";
    insererDonnee(resultat);
}




//-------------------------------------------------------------------------------------------------------
void Stats::on_calendar_selectionChanged(){

    date = ui->calendar->selectedDate();
    ui->lineEdit_date->setText(date.toString("dd/MM/yyyy"));
}





//-------------------------------------------------------------------------------------------------------
QString Stats::calculerStats(QString coordonneeUn, QString coordonneeDeux){

    QString pathHistory = getPathHistory();
    QFileInfo fichier;
    int nbFile = 0;

    //It�rateur sur le dossier pass� en param�tre
    QDirIterator dirIterator(pathHistory+"\\"+date.toString("yyyyMMdd")+"\\",QDir::Files | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while(dirIterator.hasNext()){

        fichier = dirIterator.next();

        if(fichier.lastModified().toString("hhmm") >= coordonneeUn && fichier.lastModified().toString("hhmm") <= coordonneeDeux){

            nbFile++;
        }
    }

    return(QString::number(nbFile));

}


//-------------------------------------------------------------------------------------------------------
QString Stats::getPathHistory(){

    QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);

    // On r�cup�re la valeur de warningTime du groupe [inactivite]
    QString history = settings.value("path/history","Default value history").toString();

    return(history);
}





//-------------------------------------------------------------------------------------------------------
void Stats::insererDonnee(QString resultat){

    // Cr�ation d'un objet QFile
    QFile fichierEntree(QCoreApplication::applicationDirPath()+ "//stats.html");

    // On ouvre notre fichier en lecture seule et on v�rifie l'ouverture
    if (!fichierEntree.open(QIODevice::ReadWrite | QIODevice::Text)){

        QMessageBox::information(NULL,"Probl�me ouverture fichier stats.html","Le fichier stats.html n'a pas pu �tre ouvert !");
    }

    // Cr�ation d'un objet QTextStream � partir de notre objet QFile
    QTextStream fluxEntrant(&fichierEntree);



    QFile fichierSortie(QCoreApplication::applicationDirPath()+"//stats_true.html");

    if (!fichierSortie.open(QIODevice::ReadWrite | QIODevice::Text)){

        QMessageBox::information(NULL,"Probl�me ouverture fichier connexion_true.html","Le fichier connexion.html n'a pas pu �tre cr�� !");
    }


    QTextStream fluxSortant(&fichierSortie);

    int i = 1;
    QString ligne;

    while( ! fluxEntrant.atEnd() ){


        if(i == 7){

            ligne = fluxEntrant.readLine();
            fluxSortant << ligne.insert(12,resultat) << endl;

        }
        else{

            ligne = fluxEntrant.readLine();
            fluxSortant << ligne << endl;
        }
        i++;
    }

    ui->webView->load(QCoreApplication::applicationDirPath()+"//stats_true.html");
    ui->webView->show();

}



//-------------------------------------------------------------------------------------------------------
void Stats::on_btn_day_clicked(){

    ui->lineEdit_date2->setVisible(false);
    ui->lineEdit_date->setText(date.currentDate().toString("dd/MM/yyyy"));

    date.currentDate().toString("yyyyMMdd");
}



//-------------------------------------------------------------------------------------------------------
void Stats::on_btn_month_clicked(){

    ui->lineEdit_date2->setVisible(true);
}
