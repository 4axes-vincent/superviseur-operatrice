#ifndef CONTROLEUR_H
#define CONTROLEUR_H

#include <QString>
#include <QFileDialog>
#include <QDirIterator>
#include <QDir>
#include <QMessageBox>

class Controleur
{
public:
    Controleur(QString path_GOOD, QString path_UNKNOWN);

    void controlerStructureGood();
    void controlerStructureUnknown();
    void afficherWarning(QString titre, QString msg);

private:

    QString pathG;
    QString pathU;

};

#endif // CONTROLEUR_H
