#include "Correcteur.h"
#include "ui_Correcteur.h"

Correcteur::Correcteur(QSqlDatabase db, QString nomOpe, QString VM, QString dateCo, QString heureCo, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Correcteur)
{
    ui->setupUi(this);
    this->setWindowTitle ("Correcteur");

    db_correcteur = db;
    operatrice = nomOpe;
    numVM = VM;
    date_connexion = dateCo;
    heure_deconnexion = heureCo;


    //Initalise les champs connu [DATECO & HEURECO]
    ui->lineEdit_date_connexion->setText (dateCo);
    ui->lineEdit_heure_connexion->setText (heureCo);
    ui->lineEdit_date_connexion->setReadOnly (true);
    ui->lineEdit_heure_connexion->setReadOnly (true);


    //Propose automatiquement la date & heure syst�me au moment T
    QDateTime dt;
    ui->lineEdit_date_deconnexion->setText ( dt.currentDateTime ().toString ( "dd/MM/yyyy" ) );
    ui->lineEdit_heure_deconnexion->setText ( dt.currentDateTime ().toString ( "hh:mm:ss" ) );

}










//-------------------------------------------------------------------------------------------------------
Correcteur::~Correcteur(){
    delete ui;
}










//-------------------------------------------------------------------------------------------------------
/*
  SLOT : lorsque l'utilisateur valide le formulaire
  N�cessite : date de d�connexion & heure de d�connexion non vide
  MAJ de l'enregistrement concern� dans la base de donn�es
  */
void Correcteur::on_btn_valider_clicked(){

    //Si dateDeco & heureDeco non vide
    if( ui->lineEdit_date_deconnexion->text ().isEmpty () && ui->lineEdit_heure_deconnexion->text ().isEmpty () )
        QMessageBox::warning (NULL,"Champs invalide","Veuillez remplir les deux champs");

    else{

        //On m�morise les donn�es dateDeco & heureDeco
        QString dateDeco = ui->lineEdit_date_deconnexion->text ();
        QString heureDeco = ui->lineEdit_heure_deconnexion->text ();


        //Si le base de donn�es ne s'ouvre pas
        if( ! db_correcteur.open())
            QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion" + db_correcteur.lastError().text());

        else{

            QSqlQuery sqlQuery;
            QString requete;

            requete = "UPDATE TRACE SET DATEDECO='" + dateDeco + "' , HEUREDECO='" + heureDeco + "',SESSION='Termin�e' WHERE NOMOPE='" + operatrice + "' AND VM='" + numVM + "' AND DATECO='" + date_connexion + "' AND HEURECO='" + heure_deconnexion + "';"                               ;
            if( !sqlQuery.exec (requete) )
                QMessageBox::warning (NULL,"Incident mise � jour base de donn�e","Impossible d'ex�cuter la requ�te : " + requete + " | ERREUR : " + sqlQuery.lastError ().text () );

            else
                db_correcteur.commit ();
        }

        db_correcteur.removeDatabase ( QCoreApplication::applicationDirPath () + "\\" + QHostInfo::localHostName() );
        db_correcteur.close ();

        QMessageBox::information (NULL,"Correction","Informations enregistr�es");
        close();
    }
}










//-------------------------------------------------------------------------------------------------------
/*
  SLOT : lorsque l'utilisateur clique sur le bouton annuler
  */
void Correcteur::on_btn_annuler_clicked(){
    close();
}
