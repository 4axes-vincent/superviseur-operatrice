/********************************************************************************
** Form generated from reading UI file 'stats.ui'
**
** Created: Mon 25. Nov 16:21:55 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STATS_H
#define UI_STATS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>
#include <QtWebKit/QWebView>

QT_BEGIN_NAMESPACE

class Ui_Stats
{
public:
    QWebView *webView;
    QCalendarWidget *calendar;
    QLineEdit *lineEdit_date;
    QPushButton *btn_generateGraph;
    QLabel *label;
    QPushButton *btn_day;
    QPushButton *btn_month;
    QLineEdit *lineEdit_date2;

    void setupUi(QWidget *Stats)
    {
        if (Stats->objectName().isEmpty())
            Stats->setObjectName(QString::fromUtf8("Stats"));
        Stats->resize(1297, 751);
        webView = new QWebView(Stats);
        webView->setObjectName(QString::fromUtf8("webView"));
        webView->setGeometry(QRect(20, 280, 1261, 451));
        webView->setUrl(QUrl(QString::fromUtf8("about:blank")));
        calendar = new QCalendarWidget(Stats);
        calendar->setObjectName(QString::fromUtf8("calendar"));
        calendar->setGeometry(QRect(30, 90, 256, 155));
        lineEdit_date = new QLineEdit(Stats);
        lineEdit_date->setObjectName(QString::fromUtf8("lineEdit_date"));
        lineEdit_date->setGeometry(QRect(330, 220, 113, 20));
        btn_generateGraph = new QPushButton(Stats);
        btn_generateGraph->setObjectName(QString::fromUtf8("btn_generateGraph"));
        btn_generateGraph->setGeometry(QRect(640, 220, 111, 20));
        label = new QLabel(Stats);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(670, 30, 111, 16));
        btn_day = new QPushButton(Stats);
        btn_day->setObjectName(QString::fromUtf8("btn_day"));
        btn_day->setGeometry(QRect(340, 100, 75, 23));
        btn_month = new QPushButton(Stats);
        btn_month->setObjectName(QString::fromUtf8("btn_month"));
        btn_month->setGeometry(QRect(340, 140, 75, 23));
        lineEdit_date2 = new QLineEdit(Stats);
        lineEdit_date2->setObjectName(QString::fromUtf8("lineEdit_date2"));
        lineEdit_date2->setGeometry(QRect(490, 220, 113, 20));

        retranslateUi(Stats);

        QMetaObject::connectSlotsByName(Stats);
    } // setupUi

    void retranslateUi(QWidget *Stats)
    {
        Stats->setWindowTitle(QApplication::translate("Stats", "Form", 0, QApplication::UnicodeUTF8));
        btn_generateGraph->setText(QApplication::translate("Stats", "G\303\251n\303\251rer graphique", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Stats", "Statistique r\303\251ception", 0, QApplication::UnicodeUTF8));
        btn_day->setText(QApplication::translate("Stats", "Journ\303\251e", 0, QApplication::UnicodeUTF8));
        btn_month->setText(QApplication::translate("Stats", "Mois", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Stats: public Ui_Stats {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STATS_H
