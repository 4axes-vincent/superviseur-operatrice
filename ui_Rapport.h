/********************************************************************************
** Form generated from reading UI file 'Rapport.ui'
**
** Created: Tue 13. Jan 11:45:26 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RAPPORT_H
#define UI_RAPPORT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Rapport
{
public:
    QCalendarWidget *calendrier_rapport;
    QLabel *label_titreRapport;
    QLineEdit *lineEdit_date1;
    QLineEdit *lineEdit_date2;
    QLabel *label_date1_au_date2;
    QRadioButton *radioButton_tout;
    QRadioButton *radioButton_journee;
    QRadioButton *radioButton_periode;
    QPushButton *btn_creer_rapport;
    QProgressBar *progressBar;
    QLabel *label;

    void setupUi(QWidget *Rapport)
    {
        if (Rapport->objectName().isEmpty())
            Rapport->setObjectName(QString::fromUtf8("Rapport"));
        Rapport->resize(494, 402);
        calendrier_rapport = new QCalendarWidget(Rapport);
        calendrier_rapport->setObjectName(QString::fromUtf8("calendrier_rapport"));
        calendrier_rapport->setGeometry(QRect(80, 60, 256, 155));
        label_titreRapport = new QLabel(Rapport);
        label_titreRapport->setObjectName(QString::fromUtf8("label_titreRapport"));
        label_titreRapport->setGeometry(QRect(180, 20, 46, 13));
        lineEdit_date1 = new QLineEdit(Rapport);
        lineEdit_date1->setObjectName(QString::fromUtf8("lineEdit_date1"));
        lineEdit_date1->setGeometry(QRect(90, 250, 113, 20));
        lineEdit_date2 = new QLineEdit(Rapport);
        lineEdit_date2->setObjectName(QString::fromUtf8("lineEdit_date2"));
        lineEdit_date2->setGeometry(QRect(280, 250, 113, 20));
        label_date1_au_date2 = new QLabel(Rapport);
        label_date1_au_date2->setObjectName(QString::fromUtf8("label_date1_au_date2"));
        label_date1_au_date2->setGeometry(QRect(230, 250, 21, 16));
        radioButton_tout = new QRadioButton(Rapport);
        radioButton_tout->setObjectName(QString::fromUtf8("radioButton_tout"));
        radioButton_tout->setGeometry(QRect(370, 80, 82, 17));
        radioButton_journee = new QRadioButton(Rapport);
        radioButton_journee->setObjectName(QString::fromUtf8("radioButton_journee"));
        radioButton_journee->setGeometry(QRect(370, 130, 82, 17));
        radioButton_periode = new QRadioButton(Rapport);
        radioButton_periode->setObjectName(QString::fromUtf8("radioButton_periode"));
        radioButton_periode->setGeometry(QRect(370, 180, 82, 17));
        btn_creer_rapport = new QPushButton(Rapport);
        btn_creer_rapport->setObjectName(QString::fromUtf8("btn_creer_rapport"));
        btn_creer_rapport->setGeometry(QRect(150, 290, 181, 23));
        progressBar = new QProgressBar(Rapport);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(30, 360, 451, 23));
        progressBar->setValue(24);
        label = new QLabel(Rapport);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(140, 330, 211, 16));

        retranslateUi(Rapport);

        QMetaObject::connectSlotsByName(Rapport);
    } // setupUi

    void retranslateUi(QWidget *Rapport)
    {
        Rapport->setWindowTitle(QApplication::translate("Rapport", "Form", 0, QApplication::UnicodeUTF8));
        label_titreRapport->setText(QApplication::translate("Rapport", "Rapport", 0, QApplication::UnicodeUTF8));
        label_date1_au_date2->setText(QApplication::translate("Rapport", "Au", 0, QApplication::UnicodeUTF8));
        radioButton_tout->setText(QApplication::translate("Rapport", "Tout", 0, QApplication::UnicodeUTF8));
        radioButton_journee->setText(QApplication::translate("Rapport", "Journ\303\251e", 0, QApplication::UnicodeUTF8));
        radioButton_periode->setText(QApplication::translate("Rapport", "P\303\251riode", 0, QApplication::UnicodeUTF8));
        btn_creer_rapport->setText(QApplication::translate("Rapport", "Cr\303\251er et envoyer rapport", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Rapport: public Ui_Rapport {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RAPPORT_H
