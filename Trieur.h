#ifndef SUPERVISEUR_COMPTER_TRIER_H
#define SUPERVISEUR_COMPTER_TRIER_H

#include <vector>
#include <string>
#include <iostream>
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <QMessageBox>
#include <QWidget>
#include <QString>
#include <QStringList>
#include <QFileInfo>
#include <QDateTime>
#include <QStatusBar>
#include <QList>
#include <QStringList>
#include <QTextStream>
#include <QProcess>
#include <QDirIterator>
#include <QDesktopServices>
#include <QUrl>
#include <QVector>
#include <QSettings>
#include <QCoreApplication>
#include <QProgressBar>


using namespace std;

class Trieur
{
    public:

        Trieur(QString path_GOOD, QString path_UNKNOWN, QProgressBar *bar);

        QString getPathFichierIni();

        QList<QStringList> trierErreur();
        QList<QStringList> trierErreurDate(QString date);
        QList<QStringList> trierErreurPeriode(QString date_1, QString date_2);

        QList<QStringList> trierOK();
        QList<QStringList> trierOKDate(QString date);
        QList<QStringList> trierOKPeriode(QString date_1, QString date_2);

        QList<QStringList> trierNR();
        QList<QStringList> trierNRDate(QString date);
        QList<QStringList> trierNRPeriode(QString date_1, QString date_2);

        QList<QStringList> trierOK_MANUEL();
        QList<QStringList> trierOK_MANUELDate(QString date);
        QList<QStringList> trierOK_MANUELPeriode(QString date_1, QString date_2);

        QList<QStringList> trierRecv();
        QList<QStringList> trierRecvDate(QString date);
        QList<QStringList> trierRecvPeriode(QString date_1, QString date_2);

        void trierListe (QList<QStringList> &liste);


private:

        QProgressBar *progressBar;

        QString pathG;
        QString pathU;

        QFileInfo fichier;

    };

#endif // SUPERVISEUR_COMPTER_TRIER_H
