HEADERS += \
    Identification.h \
    mainwindow.h \
    Trieur.h \
    Configuration.h \
    Rapport.h \
    Controleur.h \
    Correcteur.h \
    dial_pdfscrutre.h

SOURCES += \
    Main.cpp \
    Identification.cpp \
    Mainwindow.cpp \
    Trieur.cpp \
    Configuration.cpp \
    Rapport.cpp \
    Controleur.cpp \
    Correcteur.cpp \
    dial_pdfscrutre.cpp



FORMS += \
    Identification.ui \
    Mainwindow.ui \
    Rapport.ui \
    Correcteur.ui \
    dial_pdfscrutre.ui

QT += network

RC_FILE = ressource.rc

QT += sql

QT += webkit






