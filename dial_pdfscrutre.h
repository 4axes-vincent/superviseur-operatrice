#ifndef DIAL_PDFSCRUTRE_H
#define DIAL_PDFSCRUTRE_H

#include <QWidget>
#include <QStringList>
#include <QSettings>
#include <QMessageBox>
#include <QString>
#include <QCoreApplication>
#include <QStringBuilder>
#include <QMessageBox>

#include "Configuration.h"

namespace Ui {
class Dial_PdfScrutre;
}

class Dial_PdfScrutre : public QWidget
{
    Q_OBJECT
    
public:
    explicit Dial_PdfScrutre(QWidget *parent = 0);
    ~Dial_PdfScrutre();

    void importerPdfScrute();
    void debugger(QString titre, QString message);
    
private slots:
    void on_lineEdit_recherche_textChanged(const QString &arg1);

    void on_tableWidget_cellClicked(int row, int column);

    void on_btn_quitter_clicked();

    void on_btn_valider_clicked();

    void on_radioButton_mutuelleActive_clicked();

    void on_radioButton_mutuelleInactive_clicked();

    void on_radioButton_RAZ_clicked();

private:
    Ui::Dial_PdfScrutre *ui;

    QString mutuelleSelectionnee;
    QString activiteMutuelle;
};

#endif // DIAL_PDFSCRUTRE_H
