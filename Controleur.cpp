#include "Controleur.h"

Controleur::Controleur(QString path_GOOD, QString path_UNKNOWN){

    pathG = path_GOOD;
    pathU = path_UNKNOWN;
}



//--------------------------------------------------------------------------------------------------------------------------
void Controleur::controlerStructureGood (){

    //Variables
    QFileInfoList entryList;
    QDir dossier;
    QFileInfo info;

    //Regex pour dossier de mutuelle
    QRegExp reg("^\.{9,10}_");

    QStringList filtre;
    filtre << "OK" << "NR" << "OK_MANUEL" << "ERREUR" << "*";

    //D�claration d'un it�rateur sur dossier dans le PATHG, sous-dossier sauf "." et ".."
    QDirIterator dirOC(pathG, filtre, QDir::Dirs | QDir::NoDotAndDotDot , QDirIterator::Subdirectories);

    //Tant qu'il y a toujours des dossiers
    while( dirOC.hasNext () ){

        //On r�cup�re les informations
        info = dirOC.next ();
        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        //Si le r�pertoire est un dossier de mutuelle
        if( info.baseName ().contains (reg) ){

            if( entryList.count () < 3 )
                afficherWarning (info.baseName (), "Dossier manquant dans " + info.absoluteFilePath ());
            else if( entryList.count () > 3 )
                afficherWarning (info.baseName (), "Dossier en trop dans " + info.absoluteFilePath ());
        }
        //Si le r�pertoire en cours est ERREUR
        else if( info.baseName () == "ERREUR" ){

            if( entryList.count () < 2 )
                afficherWarning ( "Nombre de dossier dans ERREUR","Un ou des dossiers sont manquant dans " + info.absoluteFilePath () );
        }
        //Si le r�pertoire en cours est NR
        else if( info.baseName ().contains ("NR") || info.baseName ().contains ("OK_MANUEL") || info.baseName ().contains ("OK") ){

            if( entryList.count () != 0 )
                afficherWarning ("Dossier pr�sent dans " + info.baseName (),"Un ou des dossiers sont pr�sents dans " + info.absoluteFilePath () );
        }
    }
}


















//--------------------------------------------------------------------------------------------------------------------------
void Controleur::controlerStructureUnknown (){



    //Variables
    QFileInfoList entryList;
    QDir dossier;
    QFileInfo info;

    //Regex pour dossier de mutuelle
    QRegExp reg("^\.{9,10}_");

    QStringList filtre;
    filtre << "NR" << "OK_MANUEL" << "*" ;

    //D�claration d'un it�rateur sur dossier dans le PATHG, sous-dossier sauf "." et ".."
    QDirIterator dirOC(pathU, filtre, QDir::Dirs | QDir::NoDotAndDotDot , QDirIterator::Subdirectories);

    //Tant qu'il y a toujours des dossiers
    while( dirOC.hasNext () ){

        //On r�cup�re les informations
        info = dirOC.next ();
        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

        //Si le r�pertoire est un dossier de mutuelle
        if( info.baseName ().contains (reg) ){

            if( entryList.count () < 2 )
                afficherWarning ( info.baseName (), "Dossier manquant dans " + info.absoluteFilePath ());
        }
        else if( info.baseName ().contains ("OK_MANUEL") || info.baseName ().contains ("NR") || info.baseName ().contains ("TMP_") ){

            if( entryList.count () != 0 )
                afficherWarning ("Dossier pr�sent","Un ou des dossiers sont pr�sents dans " + info.absoluteFilePath () );
        }
    }
}










//-------------------------------------------------------------------------------------------
void Controleur::afficherWarning (QString titre, QString msg){

    QMessageBox::warning (NULL,titre,msg);
}
