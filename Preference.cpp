#include "Preference.h"
#include "ui_preference.h"

Preference::Preference(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Preference)
{
    ui->setupUi(this);
    this->setWindowTitle ("Pr�f�rence");
}

Preference::~Preference()
{
    delete ui;
}



void Preference::on_btn_annuler_2_clicked(){

    close();
}



void Preference::on_btn_valider_2_clicked(){

    if( !ui->radioButton_firefox->isChecked () && !ui->radioButton_googleChrome->isChecked () )
        QMessageBox::warning (NULL,"S�lection pr�f�rence","Veuillez s�lectionner un navigateur par d�faut");
    else{

        Configuration *config = new Configuration();

        if( ui->radioButton_firefox->isChecked () )
            config->setNavigateur("firefox");
        else if( ui->radioButton_googleChrome->isChecked () )
            config->setNavigateur ("googleChrome");

        QMessageBox::information (NULL,"Confirmation","Modification apport�e avec succ�s !");

        close();
    }

}
