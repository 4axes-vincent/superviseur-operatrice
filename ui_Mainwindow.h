/********************************************************************************
** Form generated from reading UI file 'Mainwindow.ui'
**
** Created: Fri 6. May 14:59:02 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionStatistiques_3;
    QAction *actionCalculer_et_envoyer;
    QAction *actionPr_f_rence;
    QAction *actionGestion_PDFScrute;
    QAction *actionMise_jour;
    QWidget *centralwidget;
    QLabel *label_nom_fenetre;
    QLineEdit *lineEdit_arecon;
    QPushButton *pushButton_refresh;
    QProgressBar *progressBar;
    QPushButton *btn_good;
    QPushButton *btn_UnknownNIN;
    QLineEdit *line_good;
    QLineEdit *line_unknown;
    QListWidget *liste_oc;
    QCalendarWidget *calendier;
    QLineEdit *line_date1;
    QLabel *label_Au;
    QLineEdit *line_date2;
    QFrame *line;
    QFrame *line_2;
    QLineEdit *lineEdit_OK;
    QPushButton *pushButton_refresh_OK;
    QRadioButton *radioButton_tout;
    QRadioButton *radioButton_journee;
    QRadioButton *radioButton_periode;
    QPushButton *btn_ou_poper;
    QLabel *label_poper;
    QPushButton *btn_mutuelle_api;
    QLabel *label_mutuelle_API;
    QPushButton *bn_faxAttente;
    QLineEdit *lineEdit_faxAttente;
    QPushButton *btn_ouvrirMailQ;
    QPushButton *btn_relancerAPI;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuRapport;
    QMenu *menuPr_f_rence;
    QMenu *menuAaide;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(693, 554);
        QIcon icon;
        icon.addFile(QString::fromUtf8("superviseur.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionStatistiques_3 = new QAction(MainWindow);
        actionStatistiques_3->setObjectName(QString::fromUtf8("actionStatistiques_3"));
        actionCalculer_et_envoyer = new QAction(MainWindow);
        actionCalculer_et_envoyer->setObjectName(QString::fromUtf8("actionCalculer_et_envoyer"));
        actionPr_f_rence = new QAction(MainWindow);
        actionPr_f_rence->setObjectName(QString::fromUtf8("actionPr_f_rence"));
        actionGestion_PDFScrute = new QAction(MainWindow);
        actionGestion_PDFScrute->setObjectName(QString::fromUtf8("actionGestion_PDFScrute"));
        actionMise_jour = new QAction(MainWindow);
        actionMise_jour->setObjectName(QString::fromUtf8("actionMise_jour"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        label_nom_fenetre = new QLabel(centralwidget);
        label_nom_fenetre->setObjectName(QString::fromUtf8("label_nom_fenetre"));
        label_nom_fenetre->setGeometry(QRect(330, 10, 101, 16));
        lineEdit_arecon = new QLineEdit(centralwidget);
        lineEdit_arecon->setObjectName(QString::fromUtf8("lineEdit_arecon"));
        lineEdit_arecon->setGeometry(QRect(110, 320, 113, 20));
        lineEdit_arecon->setMaxLength(100000000);
        lineEdit_arecon->setReadOnly(true);
        pushButton_refresh = new QPushButton(centralwidget);
        pushButton_refresh->setObjectName(QString::fromUtf8("pushButton_refresh"));
        pushButton_refresh->setGeometry(QRect(20, 320, 81, 23));
        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(20, 460, 671, 21));
        progressBar->setValue(24);
        btn_good = new QPushButton(centralwidget);
        btn_good->setObjectName(QString::fromUtf8("btn_good"));
        btn_good->setGeometry(QRect(500, 310, 75, 23));
        btn_UnknownNIN = new QPushButton(centralwidget);
        btn_UnknownNIN->setObjectName(QString::fromUtf8("btn_UnknownNIN"));
        btn_UnknownNIN->setGeometry(QRect(500, 350, 75, 23));
        line_good = new QLineEdit(centralwidget);
        line_good->setObjectName(QString::fromUtf8("line_good"));
        line_good->setGeometry(QRect(590, 310, 61, 20));
        line_good->setReadOnly(true);
        line_unknown = new QLineEdit(centralwidget);
        line_unknown->setObjectName(QString::fromUtf8("line_unknown"));
        line_unknown->setGeometry(QRect(590, 350, 61, 20));
        line_unknown->setReadOnly(true);
        liste_oc = new QListWidget(centralwidget);
        liste_oc->setObjectName(QString::fromUtf8("liste_oc"));
        liste_oc->setGeometry(QRect(250, 260, 231, 191));
        calendier = new QCalendarWidget(centralwidget);
        calendier->setObjectName(QString::fromUtf8("calendier"));
        calendier->setGeometry(QRect(30, 60, 231, 141));
        line_date1 = new QLineEdit(centralwidget);
        line_date1->setObjectName(QString::fromUtf8("line_date1"));
        line_date1->setGeometry(QRect(30, 220, 91, 20));
        line_date1->setReadOnly(true);
        label_Au = new QLabel(centralwidget);
        label_Au->setObjectName(QString::fromUtf8("label_Au"));
        label_Au->setGeometry(QRect(140, 220, 16, 16));
        line_date2 = new QLineEdit(centralwidget);
        line_date2->setObjectName(QString::fromUtf8("line_date2"));
        line_date2->setGeometry(QRect(170, 220, 91, 20));
        line_date2->setReadOnly(true);
        line = new QFrame(centralwidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(20, 240, 641, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(20, 30, 641, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        lineEdit_OK = new QLineEdit(centralwidget);
        lineEdit_OK->setObjectName(QString::fromUtf8("lineEdit_OK"));
        lineEdit_OK->setGeometry(QRect(110, 350, 113, 20));
        lineEdit_OK->setReadOnly(true);
        pushButton_refresh_OK = new QPushButton(centralwidget);
        pushButton_refresh_OK->setObjectName(QString::fromUtf8("pushButton_refresh_OK"));
        pushButton_refresh_OK->setGeometry(QRect(20, 350, 81, 23));
        radioButton_tout = new QRadioButton(centralwidget);
        radioButton_tout->setObjectName(QString::fromUtf8("radioButton_tout"));
        radioButton_tout->setGeometry(QRect(290, 80, 82, 17));
        radioButton_journee = new QRadioButton(centralwidget);
        radioButton_journee->setObjectName(QString::fromUtf8("radioButton_journee"));
        radioButton_journee->setGeometry(QRect(290, 130, 82, 17));
        radioButton_periode = new QRadioButton(centralwidget);
        radioButton_periode->setObjectName(QString::fromUtf8("radioButton_periode"));
        radioButton_periode->setGeometry(QRect(290, 180, 82, 16));
        btn_ou_poper = new QPushButton(centralwidget);
        btn_ou_poper->setObjectName(QString::fromUtf8("btn_ou_poper"));
        btn_ou_poper->setGeometry(QRect(420, 110, 101, 23));
        label_poper = new QLabel(centralwidget);
        label_poper->setObjectName(QString::fromUtf8("label_poper"));
        label_poper->setGeometry(QRect(540, 110, 46, 21));
        btn_mutuelle_api = new QPushButton(centralwidget);
        btn_mutuelle_api->setObjectName(QString::fromUtf8("btn_mutuelle_api"));
        btn_mutuelle_api->setGeometry(QRect(420, 140, 81, 23));
        label_mutuelle_API = new QLabel(centralwidget);
        label_mutuelle_API->setObjectName(QString::fromUtf8("label_mutuelle_API"));
        label_mutuelle_API->setGeometry(QRect(520, 140, 151, 21));
        bn_faxAttente = new QPushButton(centralwidget);
        bn_faxAttente->setObjectName(QString::fromUtf8("bn_faxAttente"));
        bn_faxAttente->setGeometry(QRect(20, 380, 81, 23));
        lineEdit_faxAttente = new QLineEdit(centralwidget);
        lineEdit_faxAttente->setObjectName(QString::fromUtf8("lineEdit_faxAttente"));
        lineEdit_faxAttente->setGeometry(QRect(110, 380, 113, 20));
        btn_ouvrirMailQ = new QPushButton(centralwidget);
        btn_ouvrirMailQ->setObjectName(QString::fromUtf8("btn_ouvrirMailQ"));
        btn_ouvrirMailQ->setGeometry(QRect(130, 410, 75, 23));
        btn_relancerAPI = new QPushButton(centralwidget);
        btn_relancerAPI->setObjectName(QString::fromUtf8("btn_relancerAPI"));
        btn_relancerAPI->setGeometry(QRect(550, 490, 101, 23));
        MainWindow->setCentralWidget(centralwidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 693, 21));
        menuRapport = new QMenu(menuBar);
        menuRapport->setObjectName(QString::fromUtf8("menuRapport"));
        menuPr_f_rence = new QMenu(menuBar);
        menuPr_f_rence->setObjectName(QString::fromUtf8("menuPr_f_rence"));
        menuAaide = new QMenu(menuBar);
        menuAaide->setObjectName(QString::fromUtf8("menuAaide"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuRapport->menuAction());
        menuBar->addAction(menuPr_f_rence->menuAction());
        menuBar->addAction(menuAaide->menuAction());
        menuRapport->addAction(actionCalculer_et_envoyer);
        menuPr_f_rence->addAction(actionGestion_PDFScrute);
        menuAaide->addAction(actionMise_jour);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionStatistiques_3->setText(QApplication::translate("MainWindow", "Statistiques", 0, QApplication::UnicodeUTF8));
        actionCalculer_et_envoyer->setText(QApplication::translate("MainWindow", "Calculer et envoyer", 0, QApplication::UnicodeUTF8));
        actionPr_f_rence->setText(QApplication::translate("MainWindow", "Pr\303\251f\303\251rence", 0, QApplication::UnicodeUTF8));
        actionGestion_PDFScrute->setText(QApplication::translate("MainWindow", "Gestion PDFScrute", 0, QApplication::UnicodeUTF8));
        actionMise_jour->setText(QApplication::translate("MainWindow", "Mise \303\240 jour", 0, QApplication::UnicodeUTF8));
        label_nom_fenetre->setText(QApplication::translate("MainWindow", "SuperViseur v3.8.5", 0, QApplication::UnicodeUTF8));
        pushButton_refresh->setText(QApplication::translate("MainWindow", "ERREUR", 0, QApplication::UnicodeUTF8));
        btn_good->setText(QApplication::translate("MainWindow", "GoodNIN", 0, QApplication::UnicodeUTF8));
        btn_UnknownNIN->setText(QApplication::translate("MainWindow", "UnknownNIN", 0, QApplication::UnicodeUTF8));
        label_Au->setText(QApplication::translate("MainWindow", "au ", 0, QApplication::UnicodeUTF8));
        pushButton_refresh_OK->setText(QApplication::translate("MainWindow", "R\303\251ceptionn\303\251", 0, QApplication::UnicodeUTF8));
        radioButton_tout->setText(QApplication::translate("MainWindow", "Tout", 0, QApplication::UnicodeUTF8));
        radioButton_journee->setText(QApplication::translate("MainWindow", "Journ\303\251e", 0, QApplication::UnicodeUTF8));
        radioButton_periode->setText(QApplication::translate("MainWindow", "P\303\251riode", 0, QApplication::UnicodeUTF8));
        btn_ou_poper->setText(QApplication::translate("MainWindow", "O\303\271 est le poper ?", 0, QApplication::UnicodeUTF8));
        label_poper->setText(QString());
        btn_mutuelle_api->setText(QApplication::translate("MainWindow", "O\303\271 est l'API ?", 0, QApplication::UnicodeUTF8));
        label_mutuelle_API->setText(QString());
        bn_faxAttente->setText(QApplication::translate("MainWindow", "Fax en attente", 0, QApplication::UnicodeUTF8));
        btn_ouvrirMailQ->setText(QApplication::translate("MainWindow", "Ouvrir", 0, QApplication::UnicodeUTF8));
        btn_relancerAPI->setText(QApplication::translate("MainWindow", "Relancer API", 0, QApplication::UnicodeUTF8));
        menuRapport->setTitle(QApplication::translate("MainWindow", "Rapport", 0, QApplication::UnicodeUTF8));
        menuPr_f_rence->setTitle(QApplication::translate("MainWindow", "Edition", 0, QApplication::UnicodeUTF8));
        menuAaide->setTitle(QApplication::translate("MainWindow", "Aide", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
