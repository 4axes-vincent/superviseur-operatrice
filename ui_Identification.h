/********************************************************************************
** Form generated from reading UI file 'Identification.ui'
**
** Created: Tue 13. Jan 11:45:26 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IDENTIFICATION_H
#define UI_IDENTIFICATION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_Identification
{
public:
    QDialogButtonBox *buttonBox_nomUser;
    QComboBox *comboBox_nomUser;
    QLabel *label_titre;

    void setupUi(QDialog *Identification)
    {
        if (Identification->objectName().isEmpty())
            Identification->setObjectName(QString::fromUtf8("Identification"));
        Identification->resize(255, 186);
        Identification->setAcceptDrops(false);
        QIcon icon;
        icon.addFile(QString::fromUtf8("login2.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Identification->setWindowIcon(icon);
        Identification->setModal(true);
        buttonBox_nomUser = new QDialogButtonBox(Identification);
        buttonBox_nomUser->setObjectName(QString::fromUtf8("buttonBox_nomUser"));
        buttonBox_nomUser->setGeometry(QRect(50, 130, 156, 23));
        buttonBox_nomUser->setOrientation(Qt::Horizontal);
        buttonBox_nomUser->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        comboBox_nomUser = new QComboBox(Identification);
        comboBox_nomUser->setObjectName(QString::fromUtf8("comboBox_nomUser"));
        comboBox_nomUser->setEnabled(true);
        comboBox_nomUser->setGeometry(QRect(50, 70, 161, 31));
        QFont font;
        font.setPointSize(10);
        comboBox_nomUser->setFont(font);
        label_titre = new QLabel(Identification);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(90, 30, 111, 16));

        retranslateUi(Identification);
        QObject::connect(buttonBox_nomUser, SIGNAL(accepted()), Identification, SLOT(accept()));
        QObject::connect(buttonBox_nomUser, SIGNAL(rejected()), Identification, SLOT(reject()));

        QMetaObject::connectSlotsByName(Identification);
    } // setupUi

    void retranslateUi(QDialog *Identification)
    {
        Identification->setWindowTitle(QApplication::translate("Identification", "Identification", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("Identification", "Identifiez-vous !", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Identification: public Ui_Identification {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IDENTIFICATION_H
